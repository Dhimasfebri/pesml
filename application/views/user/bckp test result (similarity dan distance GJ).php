            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                      <?= $this->session->flashdata('message'); ?>
                        <h4 class="page-title">Recomendation Page</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Recomendation page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                   <!--  <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-danger text-white" target="_blank">Upgrade to Pro</a>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ==============================================================
            
            ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                        
                            <!-- nyoba manual/statis -->
                            <?php
                                $user = ['1', '3', '3', '1'];
                                $DC01 = ['2', '3', '4', '1'];
                                $DC02 = ['1', '4', '3', '1'];
                                $DC03 = ['3', '3', '3', '1'];
                                $DC04 = ['3', '2', '5', '1'];
                                $DC05 = ['2', '2', '4', '1'];
                                $DC06 = ['3', '2', '4', '1'];
                                $DC07 = ['2', '1', '3', '1'];
                                $DC08 = ['1', '3', '3', '1'];
                                $DC09 = ['2', '1', '4', '1'];
                                $DC10 = ['1', '1', '2', '1'];

                                $player = [$DC01,$DC02,$DC03,$DC04,$DC05,$DC06,$DC07,$DC08,$DC09,$DC10];
                               	$users = [$user];

                                //menghitung distance
                                $P_DC01 = 
                                pow(($DC01[0]-$user[0]),2) + 
                                pow(($DC01[1]-$user[1]),2) + 
                                pow(($DC01[2]-$user[2]),2) + 
                                pow(($DC01[3]-$user[3]),2);

 
                                $P_DC02 = 
                                pow(($DC02[0]-$user[0]),2) + 
                                pow(($DC02[1]-$user[1]),2) + 
                                pow(($DC02[2]-$user[2]),2) + 
                                pow(($DC02[3]-$user[3]),2);

                                $P_DC03 = 
                                pow(($DC03[0]-$user[0]),2) + 
                                pow(($DC03[1]-$user[1]),2) + 
                                pow(($DC03[2]-$user[2]),2) + 
                                pow(($DC03[3]-$user[3]),2);

                                $P_DC04 = 
                                pow(($DC04[0]-$user[0]),2) + 
                                pow(($DC04[1]-$user[1]),2) + 
                                pow(($DC04[2]-$user[2]),2) + 
                                pow(($DC04[3]-$user[3]),2);

                                $P_DC05 = 
                                pow(($DC05[0]-$user[0]),2) + 
                                pow(($DC05[1]-$user[1]),2) + 
                                pow(($DC05[2]-$user[2]),2) + 
                                pow(($DC05[3]-$user[3]),2);

                                $P_DC06 = 
                                pow(($DC06[0]-$user[0]),2) + 
                                pow(($DC06[1]-$user[1]),2) + 
                                pow(($DC06[2]-$user[2]),2) + 
                                pow(($DC06[3]-$user[3]),2);

                                $P_DC07 = 
                                pow(($DC07[0]-$user[0]),2) + 
                                pow(($DC07[1]-$user[1]),2) + 
                                pow(($DC07[2]-$user[2]),2) + 
                                pow(($DC07[3]-$user[3]),2);

                                $P_DC08 = 
                                pow(($DC08[0]-$user[0]),2) + 
                                pow(($DC08[1]-$user[1]),2) + 
                                pow(($DC08[2]-$user[2]),2) + 
                                pow(($DC08[3]-$user[3]),2);

                                $P_DC09 = 
                                pow(($DC09[0]-$user[0]),2) + 
                                pow(($DC09[1]-$user[1]),2) + 
                                pow(($DC09[2]-$user[2]),2) + 
                                pow(($DC09[3]-$user[3]),2);

                                $P_DC10 = 
                                pow(($DC10[0]-$user[0]),2) + 
                                pow(($DC10[1]-$user[1]),2) + 
                                pow(($DC10[2]-$user[2]),2) + 
                                pow(($DC10[3]-$user[3]),2);


                                ?>
                                P_DCO1 = <?=$P_DC01;?><br>
                                P_DCO2 = <?=$P_DC02;?><br>
                                P_DCO3 = <?=$P_DC03;?><br>
                                P_DCO4 = <?=$P_DC04;?><br>
                                P_DCO5 = <?=$P_DC05;?><br>
                                P_DCO6 = <?=$P_DC06;?><br>
                                P_DCO7 = <?=$P_DC07;?><br>
                                P_DCO8 = <?=$P_DC08;?><br>
                                P_DCO9 = <?=$P_DC09;?><br>
                                P_DC10 = <?=$P_DC10;?>

                               <?php

                               // menghitung similarity distance
                               $S_P_DCO1 = 1/(1+$P_DC01);
                               $S_P_DCO2 = 1/(1+$P_DC02);
                               $S_P_DCO3 = 1/(1+$P_DC03);
                               $S_P_DCO4 = 1/(1+$P_DC04);
                               $S_P_DCO5 = 1/(1+$P_DC05);
                               $S_P_DCO6 = 1/(1+$P_DC06);
                               $S_P_DCO7 = 1/(1+$P_DC07);
                               $S_P_DCO8 = 1/(1+$P_DC08);
                               $S_P_DCO9 = 1/(1+$P_DC09);
                               $S_P_DC10 = 1/(1+$P_DC10);?><br><br>

                               S_P_DCO1 = <?= $S_P_DCO1;?><br>
                               S_P_DCO2 = <?= $S_P_DCO2;?><br>
                               S_P_DCO3 = <?= $S_P_DCO3;?><br>
                               S_P_DCO4 = <?= $S_P_DCO4;?><br>
                               S_P_DCO5 = <?= $S_P_DCO5;?><br>
                               S_P_DCO6 = <?= $S_P_DCO6;?><br>
                               S_P_DCO7 = <?= $S_P_DCO7;?><br>
                               S_P_DCO8 = <?= $S_P_DCO8;?><br>
                               S_P_DCO9 = <?= $S_P_DCO9;?><br>
                               S_P_DC10 = <?= $S_P_DC10;?>

                               <?php 
                               $result= array($S_P_DCO1,$S_P_DCO2,$S_P_DCO3,$S_P_DCO4,$S_P_DCO5,$S_P_DCO6,$S_P_DCO7,$S_P_DCO8,$S_P_DCO9,$S_P_DC10);

                             
                               rsort($result);
                               echo "<br>" ;			

                               $urutan = 1;
                               foreach($result as $r ) {

							    echo "urutan 1 adalah " . $r;
							    echo "<br>" 
							    ;
							    $urutan ++;    
				}
				?> 
				<!-- nyoba otomatis/dinamis -->
				<br><br>
				<hr>
				<h1 style="color:black">NYOBA DINAMIS</h1>
				<hr>
				<br><br>
				<?php

				// untuk perhitungan similarity semua pemain
				$similarity = [];
				for ($a=0; $a < 10 ; $a++) { 

					// untuk perhitungan semua pemain
					$distance = [];
					for ($x=0; $x < 10 ; $x++) { 
						// untuk perhitungan perfitur

						$hasil = [];
           	for ($i=0; $i < 4 ; $i++) { 
       		  $jumlah = pow(($player[$x][$i]-$users[0][$i]),2);
       		  array_push($hasil, $jumlah);
           	  }

         	$total = array_sum($hasil);
         	array_push($distance, $total);
  				}

				$bobot = 1/(1+$distance[$a]);
				array_push($similarity, $bobot);
				}

				var_dump($similarity);

				rsort($similarity);
                echo "<br>" ;			

                $urutan = 1;
	            foreach($similarity as $r ) {

				    echo "urutan ".$urutan." adalah " . $r;
				    echo "<br>" 
				    ;
				$urutan ++;    
				}
					
				
                ?>





                               

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
  