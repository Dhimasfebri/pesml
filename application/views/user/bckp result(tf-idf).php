<?php
  
  if (empty($_POST)) {
   redirect("my404");
   die;
}


  $id_rating = $_POST['id'];
  $position1 = $_POST["position"];
  $height1 = $_POST["height"];
  $age1 = $_POST["age"];
  $ovr1 = $_POST["ovr"];
  $mv1 = $_POST["mv"];
  $sal1= $_POST["sal"];
  $idplayerrole1 = $_POST["idplayerrole"];
  $in21to291 = $_POST["in21to29"];
  $u211 = $_POST["u21"];
  $up291 = $_POST["up29"];
  $budget1 = $_POST["budget"];



  
// memfilter posisi pemain sesuai yang dipilih //
// preprocessing data //
  $recomendation = $this->db->query("
    SELECT *,

    CASE 
    WHEN height < 171 THEN 1
    WHEN height <= 181 THEN 2
    WHEN height <= 191 THEN 3
    WHEN height <= 201 THEN 4
    WHEN height <= 300 THEN 5
    END AS height_id,

    CASE 
    WHEN age < 22 THEN 1
    WHEN age <= 29 THEN 2
    WHEN age <= 30 THEN 3
    WHEN age <= 40 THEN 4
    WHEN age <= 50 THEN 5
    END AS age_id,

    CASE 
    WHEN overall < 80 THEN 1
    WHEN overall <= 85 THEN 2
    WHEN overall <= 90 THEN 3
    WHEN overall <= 100 THEN 4
    END AS overalle_id,

    CASE 
    WHEN market_value < 1 THEN 1
    WHEN market_value <= 20 THEN 2
    WHEN market_value <= 50 THEN 3
    WHEN market_value <= 100 THEN 4
    WHEN market_value <= 150 THEN 5
    WHEN market_value <= 300 THEN 6
    END AS market_value_id,

    CASE 
    WHEN salaries < 1 THEN 1
    WHEN salaries <= 4 THEN 2
    WHEN salaries <= 10 THEN 3
    WHEN salaries <= 25 THEN 4
    END AS salaries_id

    FROM player 

    JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
    JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
    JOIN `league` ON `team`.`id_league` = `league`.`id_league`
    JOIN `recomendation` ON `recomendation`.`id_player` = `player`.`id_player`

    WHERE `player`.`id_position` = $position1")->result();
    ?>
  
            
                  <?php
                  $total_row = count($recomendation);


      ///////////////////////////////// PERHITUNGAN TF //////////////////////////////////////////////
     //mencari data yang sama dalam fitur dataset dengan fitur input user  diulangi tiap baris data

                // data yang dicek sama akan dimasukkan ke array sebagai int '1'
                 foreach ($recomendation as $row){
                  $position[] = $row->id_position == $position1  ? 1:0;
                  $height[] = $row->height_id == $height1 ? 1:0;
                  $age[] = $row->age_id ==  $age1 ? 1:0;
                  $ovr[] = $row->overalle_id == $ovr1 ? 1:0;
                  $mv[] = $row->market_value_id == $mv1 ? 1:0;
                  $sal[] = $row->salaries_id == $sal1 ? 1:0;
                  $id_player_role[] = $row->id_player_role == $idplayerrole1 ? 1:0;
                  $in21to29[] = $row->in21to29 == $in21to291 ? 1:0;
                  $u21[] = $row->u21 == $u211  ? 1:0;
                  $up29[] = $row->up29 == $up291 ? 1:0;
                  $uang[] = $row->uang == $budget1 ? 1:0;

                 }
                              
                                                          
        /////////////////////////////////  PERTHITUNGAN IDF  //////////////////////////////////  
       // menghitung jumlah fitur 1 yang telah ditemukan sama tiap baris data, diulangi menghitung jumlah fitur 2 disetiap baris data dst. kemudian jumlah fitur dibagi dengan total fitur yang sama dengan input  //


                              // position
                $position_df = array();
                foreach ($position as $position_count) {
                  //biar jumlah term tidak kosong array ditambah 1 karena jika kosong hasil akan tidak terdefinisi
                   if($position_count == 1){
                     $position_cut = 1;
                     array_push($position_df, $position_cut);
                   }
                } 

                $size_position_df = sizeof($position_df); 
                $idf_position = $size_position_df == 0 ? '0' : log10($total_row/$size_position_df) + 1;
                

                // height
                $height_df = array();
                foreach ($position as $height_count) {
                   if($height_count == 1){
                     $height_cut = 1;
                     array_push($height_df, $height_cut);
                   }
                } 

                $size_height_df = sizeof($height_df); 
                $idf_height = $size_height_df == 0 ? '0' : log10($total_row/$size_height_df) + 1;


                // age
                $age_df = array();
                foreach ($age as $age_count) {
                   if($age_count == 1){
                     $age_cut = 1;
                     array_push($age_df, $age_cut);
                   }
                } 

                $size_age_df = sizeof($age_df); 
                $idf_age = $size_age_df == 0 ? '0' : log10($total_row/$size_age_df) + 1;


                // ovr
                $overall_df = array();
                foreach ($ovr as $overall_count) {
                   if($overall_count == 1){
                     $overall_cut = 1;
                     array_push($overall_df, $overall_cut);
                   }
                } 

                $size_overall_df = sizeof($overall_df); 
                $idf_overall = $size_overall_df == 0 ? '0' : log10($total_row/$size_overall_df) + 1;


                // mv
                $mv_df = array();
                foreach ($mv as $mv_count) {
                   if($mv_count == 1){
                     $mv_cut = 1;
                     array_push($mv_df, $mv_cut);
                   }
                } 

                $size_mv_df = sizeof($mv_df); 
                $idf_mv = $size_mv_df == 0 ? '0' : log10($total_row/$size_mv_df) + 1;


                // sal
                $sal_df = array();
                foreach ($sal as $sal_count) {
                   if($sal_count == 1){
                     $sal_cut = 1;
                     array_push($sal_df, $sal_cut);
                   }
                } 

                $size_sal_df = sizeof($sal_df); 
                $idf_sal = $size_sal_df == 0 ? '0' : log10($total_row/$size_sal_df) + 1;


                // player-role
                $id_player_role_df = array();
                foreach ($id_player_role as $id_player_role_count) {
                   if($id_player_role_count == 1){
                     $id_player_role_cut = 1;
                     array_push($id_player_role_df, $id_player_role_cut);
                   }
                } 

                $size_id_player_role_df = sizeof($id_player_role_df); 
                $idf_id_player_role = $size_id_player_role_df == 0 ? '0' : log10($total_row/$size_id_player_role_df) + 1;


                // 21-29
                $in21to29_df = array();
                foreach ($in21to29 as $in21to29_count) {
                   if($in21to29_count == 1){
                     $in21to29_cut = 1;
                     array_push($in21to29_df, $in21to29_cut);
                   }
                } 

                $size_in21to29_df = sizeof($in21to29_df); 
                $idf_in21to29 = $size_in21to29_df == 0 ? '0' : log10($total_row/$size_in21to29_df) + 1;
                

                // >21
                $u21_df = array();
                foreach ($u21 as $u21_count) {
                   if($u21_count == 1){
                     $u21_cut = 1;
                     array_push($u21_df, $u21_cut);
                   }
                } 

                $size_u21_df = sizeof($u21_df); 
                $idf_u21 = $size_u21_df == 0 ? '0' : log10($total_row/$size_u21_df) + 1;


                // >29
                $up29_df = array();
                foreach ($up29 as $up29_count) {
                   if($up29_count == 1){
                     $up29_cut = 1;
                     array_push($up29_df, $up29_cut);
                   }
                } 

                $size_up29_df = sizeof($up29_df); 
                $idf_up29 = $size_up29_df == 0 ? '0' : log10($total_row/$size_up29_df) + 1;



                // uang
                $uang_df = array();
                foreach ($uang as $uang_count) {
                   if($uang_count == 1){
                     $uang_cut = 1;
                     array_push($uang_df, $uang_cut);
                   }
                } 

                $size_uang_df = sizeof($uang_df); 
                $idf_uang = $size_uang_df == 0 ? '0' : log10($total_row/$size_uang_df) + 1;

                

  ////////////////////////////////////  HITUNG BOBOT TERM = IDF X TERM //////////////////////////////////  
                      // weight per term //

                // position
                for($i=0; $i < count($position); $i++){
                      $w_position[]=$position[$i] * $idf_position;
                      
                }           
                $total_position = [];
                array_push($total_position, $w_position);


                // height
                for($i=0; $i < count($height); $i++){
                      $w_height[]=$height[$i] * $idf_height;
                      
                }
                $total_height = [];
                array_push($total_height, $w_height);


                // age
                for($i=0; $i < count($age); $i++){
                      $w_age[]=$age[$i] * $idf_age;
                      
                }
                $total_age= [];
                array_push($total_age, $w_age);


                // overall
                for($i=0; $i < count($ovr); $i++){
                      $w_overall[]=$ovr[$i] * $idf_overall;
                      
                }
                $total_overall = [];
                array_push($total_overall, $w_overall);


                // mv
                for($i=0; $i < count($mv); $i++){
                      $w_mv[]=$mv[$i] * $idf_mv;
                      
                }
                $total_mv = [];
                array_push($total_mv, $w_mv);



                // sal
                for($i=0; $i < count($sal); $i++){
                      $w_sal[]=$sal[$i] * $idf_sal;
                      
                }
                $total_sal = [];
                array_push($total_sal, $w_sal);



                //id_role
                for($i=0; $i < count($id_player_role); $i++){
                      $w_id_player_role[]=$id_player_role[$i] * $idf_id_player_role;
                      
                }
                $total_id_player_role = [];
                array_push($total_id_player_role, $w_id_player_role);


                //21-29

                for($i=0; $i < count($sal); $i++){
                      $w_in21to29[]=$in21to29[$i] * $idf_in21to29;
                      
                }
                $total_in21to29 = [];
                array_push($total_in21to29, $w_in21to29);


                //ud-21
                for($i=0; $i < count($u21); $i++){
                      $w_u21[]=$u21[$i] * $idf_u21;
                      
                }
                $total_u21 = [];
                array_push($total_u21, $w_u21);

                //>29
                for($i=0; $i < count($up29); $i++){
                      $w_up29[]=$up29[$i] * $idf_up29;
                      
                }
                $total_up29 = [];
                array_push($total_up29, $w_up29);


                //uang
                for($i=0; $i < count($uang); $i++){
                      $w_uang[]=$uang[$i] * $idf_uang;
                      
                }
                $total_uang = [];
                array_push($total_uang, $w_uang);

   
                  
  //////////////////////////////  PERHITUNGAN TOTAL BOBOT DOKUMEN ////////////////////////////////
      //perhitungan bobot tiap fitur dalam 1 dokumen = total bobot tiap dokumen / baris data //                 
                $z = 0; 
                foreach($recomendation as $row){ 
                $total = $w_position[$z] + $w_height[$z] + $w_age[$z] + $w_overall[$z] + $w_mv[$z] + $w_sal[$z] + $w_id_player_role[$z] + $w_in21to29[$z] + $w_u21[$z] + $w_up29[$z] + $w_uang[$z] ;
                $sort[$row->id_player] = $total; // array yang berisi data tiap id_player yang punya nilai bobot   
                $z++;
                }

                
                

  ///////////////////////////////// HASIL REKOMENDASI ////////////////////////////////////
    // tampilkan hasil rekomendasi berdasarkan bobot terbesar ke terkecil //



                arsort($sort);// sorting array bobot dan id player berdasarkan bobot paling besar
                // foreach ($sort as $key => $value){                
                // $row1 = $this->db->query("SELECT *  FROM recomendation WHERE id_player = '$key'")->row();
                // echo $key .' - '. $value .' - '.$row1->name .'<br>';  
                // }     
                
        ?>
                <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 style = "color:black"class="page-title">Home</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Recomendation page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <!-- <div class="card">
                            <div class="card-body"> -->
                                           
                        <center class="m-b-20">
                          <h1 style="color:black" class="card-title m-t-10">Recomendation Player</h1>
                            <h5 class="card-title m-t-10">Master League PES 2020</h5>
                              <h12 class="card-title m-t-10">- Last Transfer Januari 2020 -</h12><br><br>
                                
                                <a href= "<?=base_url('user/recomendation');?>"><button class="btn btn-primary ">Try Again ?</button></a>
                        </center>

                        <button class="btn btn-primary ">#1</button>
                    </div> 

              <!-- ambil fitur data player dari database berdasarkan data player yang telah direkomendasikan dan diurutkan bobot tertinggi -->
              <?php
              $inc = 0;
                foreach ($sort as $key => $value): 
                  $inc++;
                  $row1 = $this->db->query("SELECT * FROM `player` JOIN `recomendation` ON `player`.`id_player` = `recomendation`. `id_player` JOIN `team` ON `player`.`id_team` = `team`.`id_team` JOIN `position` ON `player`.`id_position` = `position`.`id_position` JOIN `league` ON `team`.`id_league` = `league`.`id_league` WHERE `player`.`id_player` = '$key' limit 5")->row();
                  ?>

                    <a href="<?=base_url('league/'.$row1->league_name.'/'.$row1->team_name.'/players/'.$row1->name);?>">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="card1">
                            <div class="card1-body">
                              <center class="m-t-30"><img src="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->name).'.png');?>" alt="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->name).'.png');?>"width="60%"/>
                                    <h4 class="card-title m-t-10"><?= $row1->name;?></h4>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i><font class="font-medium">Overall<hr class="new5"><h4><?=$row1->overall;?></h4></font></a></div>
                                        <div class="col-2">
                                            <div class="click-to-top"><a href ="<?= base_url('league/'.$row1->league_name.'/'.$row1->team_name)?>"><img  src="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->team_name).'.png');?>" width="30 %"><span><?=$row1->team_name?></span>
                                            </div>
                                        </div>
                                        <div class="col-6"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">Market Value<hr class ="new5">€ <?=$row1->market_value;?> million </font></a></div>

                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                    <?php
                    if($inc == 4){
                        break;
                     }
                     // JIKA BOBOT KELAS ADALAH 0 TIDAK DITAMPILKAN   
                     if($value == 0){
                        break;
                     }
                     ?>
                    <?php endforeach;?>
                    </a>
                  </div>
                </div>

              <!-- ambil data player rekomendasi no 1 -->
              <?php 
               $yaha = 0;
               foreach ($sort as $key => $value): 
                $yaha++;
                $row2 = $this->db->query("SELECT * FROM `player` JOIN `recomendation` ON `player`.`id_player` = `recomendation`. `id_player` JOIN `team` ON `player`.`id_team` = `team`.`id_team` JOIN `position` ON `player`.`id_position` = `position`.`id_position` JOIN `league` ON `team`.`id_league` = `league`.`id_league` WHERE `player`.`id_player` = '$key' limit 5")->row_array();
                if($yaha == 1){
                        break;
                     }
                ?>
                <?php endforeach;?>
               
              


              <?php if($rating['id_rating'] != $id_rating) :?>                 
                  <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Rate This Recomendation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                  
                        <div class="modal-body">                  
                        <center>
                        <form action="<?=base_url('user/rating');?>" method="post" class="needs-validation" novalidate>
                        <input type="hidden" name="id_player" id="id_player" value="<?=$row2['id_player'];?>">
                        <input type="hidden" name="id_rating" id="id_rating" value="<?=$id_rating;?>">
                        <input type="hidden" name="email" id="email" value="<?=$user['email'];?>">
                        <ul class="list-inline">
                          <select class="gl-star-rating" id="rate" name="rate" required>
                              <option value="">Select a rating</option>
                              <option value="5">Excellent</option>
                              <option value="4">Very Good</option>
                              <option value="3">Average</option>
                              <option value="2">Poor</option>
                              <option value="1">Terrible</option>
                          </select>
                        </ul>
                        </center>                   
                      </div>
                      <div class="modal-footer">   
                        <button type = "submit" class="btn btn-success btn-sm"><h6 class="m-b-0 font-16">Rate & Save</h6></button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Skip</button><br>
                        <p style="color: red">note: save and rate will return you to the recommendation page, if you haven't seen the detailed data of each player, please click the skip button</p>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              <?php endif ?>



      <script src="<?=base_url('assets/');?>dist/star-rating.min.js"></script>
      <script>
        var starRatingControl = new StarRating('.gl-star-rating',{
          maxStars: 5

      });
      </script>
   
    
   
                
 

  

                
              

  
 