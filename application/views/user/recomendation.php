            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                      <?= $this->session->flashdata('message'); ?>
                        <h4 style = "color:black"class="page-title">Recomendation Page</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Recomendation page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
       
           <!--  ambil data umur dari tim yang dipilih -->
            <?php 
            $id_team = $myteam['id_team'];

            $totalup29 = $this->db->query("SELECT count(name) as totalup29 from `player` where id_team = $id_team and age > 29 ")->row_array();
            $total21to29 = $this->db->query("SELECT count(name) as total21to29 from `player` where id_team = $id_team and age between 21 and 29 ")->row_array();
            $totalu21 = $this->db->query("SELECT count(name) as totalu21 from `player` where id_team = $id_team and age < 21 ")->row_array();
              
            ?>

            <div class="container-fluid">
              <!-- ============================================================== -->
              <!-- Start Page Content -->
              <!-- ============================================================== -->
              <div class="row justify-content-md-center">
                <div class="col-lg-6 md-6 sm-12">
                    <div class="card">
                        <div class="card-header bg-dark">
                            <h2 class="text-center text-white">Your Squad data</h2>
                        </div>    
                        <div class="card2-body">
                                              
                          <center><img src="<?=base_url('assets/images/team/'.$myteam['team_name'].'/'.$myteam['team_name'].'.png');?>" class="img-fluid rounded-circle  mb-3" width="128" height="128"/></center>               
                          <h3 class="card-title m-t-10 text-center text-white"><?=$myteam['team_name'];?></h3>
                          <div class="table-responsive">   
                            <table class="table">
                              <thead class = "table table-hover">
                                <tr>
                                  <th><center>Transfer Budget <i class="far fa-money-bill-alt"></i></center></th>
                                  <th><center>Salary Budget <i class="fas fa-hand-holding-usd"></i></center></th>
                                  <th>Age < 21</th>
                                  <th>Age 21-29</th>
                                  <th>Age > 29</th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                                <tr>                               
                                  <td class="text-white"><center>€ <?=number_format($myteam['transfer_budget']);?></center></td>
                                  <td class="text-white"><center>€ <?=number_format($myteam['salary_budget']);?></center></td>

                                  <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>

                                    <td class="text-white"><center><?=$totalu21['totalu21'];?> player</center></td>
                                    <td class="text-white"><center><?=$total21to29['total21to29'];?> player</center></td>
                                    <td class="text-white"><center><?=$totalup29['totalup29'];?> player</center></td>

                                  <?php else:?>

                                    <td class="text-white"><center><?=$myteam['total_age_less_21'];?> player</center></td>
                                    <td class="text-white"><center><?=$myteam['total_age_21to29'];?> player</center></td>
                                    <td class="text-white"><center><?=$myteam['total_age_over_29'];?> player</center></td>
                                  <?php endif;?>
                                  
                                </tr>
                                
                                  <center><a  href="javascipt:void(0)" data-toggle="modal" data-target="#editModal<?= $myteam['id_team'] ?>"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a></center><br>
                               
                              </tbody>
                            </table>
                          </div>
                          
                          <hr class="new5">                     

                          
                            <!-- buat id rating -->
                            <?php $random = rand(1,100).substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 3);?>
                            <!-- buat id rating -->
                            <h3 class="text-center text-white">Which player do you want?</h3><br>
                            <form action ="<?=base_url('user/recomendation/result');?>" method="post" class="needs-validation" novalidate>

                             
                            <input type="hidden" name="id" id="id" value="<?=$random;?>">
                            <input type="hidden" name="transfer_budget" id="id" value="<?=$myteam['transfer_budget'];?>">
                            <input type="hidden" name="salary_budget" id="id" value="<?=$myteam['salary_budget'];?>">
                             <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>
                 
                              <input type="hidden" name="total_age_less_21" id="id" value="<?=$totalu21['totalu21'];?>">
                              <input type="hidden" name="total_age_21to29" id="id" value="<?=$total21to29['total21to29'];?>">
                              <input type="hidden" name="total_age_over_29" id="id" value="<?=$totalup29['totalup29'];?>">

                             <?php else : ?>
                              <input type="hidden" name="total_age_less_21" id="id" value="<?=$myteam['total_age_less_21'];?>">
                              <input type="hidden" name="total_age_21to29" id="id" value="<?=$myteam['total_age_21to29'];?>"> 
                              <input type="hidden" name="total_age_over_29" id="id" value="<?=$myteam['total_age_over_29'];?>">

                             <?php endif;?>: 
                             
                             <div class="form-group row">
                               <label for="inputPassword3" class="col-sm-6 col-form-label text-white">Position</label>
                                <div class="col-sm-6">
                                 <select class="custom-select" name="position" required>
                                   <option value="">Choose Position</option>
                                   <option value="1">GK (GoalKeeper)</option>
                                   <option value="2">CB (Centre Back)</option>
                                   <option value="3">RB (Right Back)</option>
                                   <option value="4">LB (Left Back)</option>
                                   <option value="5">DMF (Defensive Midfielder)</option>
                                   <option value="6">CMF (Centre Midfielder)</option>
                                   <option value="7">RMF (Right Midfielder)</option>
                                   <option value="8">LMF (Left Midfilder)</option>
                                   <option value="9">AMF (Attacking Midfielder)</option>
                                   <option value="10">RWF (Right Wing Foward)</option>
                                   <option value="11">LWF (Left Wing Foward)</option>  
                                   <option value="12">CF (Centre Foward)</option>
                                   <option value="13">SS (Second Striker)</option>
                                 </select>
                                  <div class="valid-feedback">
                                        Looks,Good!!
                                  </div>
                                  <div class="invalid-feedback">
                                       Choose Position!.
                                  </div>
                                </div>
                             </div>
                             
                              <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-6 col-form-label text-white">Age</label>
                                <div class="col-sm-6">
                                  <select class="custom-select" name="age" required>
                                   <option value="">Choose Age</option>
                                   <option value="Auto">Auto</option>
                                   <option value=1>(16-21)</option>
                                   <option value=2>(23-29)</option>
                                   <option value=3>(30-40)</option>                  
                                   <option value=4>(41-50)</option>                                    
                                 </select>
                                 <div class="valid-feedback">
                                        Looks,Good!!
                                  </div>
                                  <div class="invalid-feedback">
                                       Choose Age!.
                                  </div>
                                </div>
                              </div>                     
                              
                              <div class="form-group row">
                                <div class="col-sm-12">
                                  <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="mdi mdi-search-web"></i> Search Player</button>

                                </div>
                              </div>
                            </form>
                           <!--  <?php var_dump($_POST);?> -->

                        </div>
                    </div>
                </div>
              </div>



              <div class="modal fade" id="editModal<?= $myteam['id_team'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form method="post" action="<?= base_url('user/editMySquad/');?>" class="needs-validation" novalidate>
                        <div class="form-row">
                          <div class="col-md-6 mb-3">
                            <label for="validationServer01">Transfer Budget min (5M Euro)</label>
                            <input type="number" min="5000000" class="form-control" id="transfer_budget" name="transfer_budget"placeholder="transfer_budget" value="<?=$myteam['transfer_budget'];?>" required>
                            <div class="valid-feedback">
                              Looks,Good!!
                            </div>
                            <div class="invalid-feedback">
                             Required transfer budget(min 5M Euro)!
                            </div>
                          </div>
                          <div class="col-md-6 mb-3">
                            <label for="validationServer01">Salary Budget min (600K Euro)</label>
                            <input type="number" min="600000" class="form-control" id="salary_budget" name="salary_budget"placeholder="salary_budget" value="<?=$myteam['salary_budget'];?>" required>
                            <div class="valid-feedback">
                              Looks,Good!!
                            </div>
                            <div class="invalid-feedback">
                             Required salary budget (min 600K Euro)!.
                            </div>
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="col-md-4 mb-3">
                            <label for="validationServer03">Total age < 21</label>
                            <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>
                              <input type="number" name="total_age_less_21" max="25" value="<?=$totalu21['totalu21'];?>" class="form-control" id="validationServer03"  required>
                            <?php else:?>
                              <input type="number" name="total_age_less_21" max="25" value="<?=$myteam['total_age_less_21'];?>" class="form-control" id="validationServer03"  required>
                            <?php endif;?>  
                            <div class="valid-feedback">
                              Looks,Good!!
                            </div>
                            <div class="invalid-feedback">
                             Required total age max 25!
                            </div>
                          </div>
                          <div class="col-md-4 mb-3">
                            <label for="validationServer04">Total age between 21-29</label>
                            <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>
                              <input type="number" name="total_age_21to29" max="25" value="<?=$total21to29['total21to29'];?>" class="form-control" id="validationServer05" required>
                            <?php else:?>
                              <input type="number" name="total_age_21to29" max="25" value="<?=$myteam['total_age_21to29'];?>" class="form-control" id="validationServer03"  required>
                            <?php endif;?> 
                            <div class="valid-feedback">
                              Looks,Good!!
                            </div>
                            <div class="invalid-feedback">
                             Required total age max 25!
                            </div> 
                          </div>
                          <div class="col-md-4 mb-3">
                            <label for="validationServer05">Total age > 29</label>
                            <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>
                              <input type="number" name="total_age_over_29" max="25" value="<?=$totalup29['totalup29'];?>" class="form-control" id="validationServer05" required>
                            <?php else:?>
                              <input type="number" name="total_age_over_29"  max="25" value="<?=$myteam['total_age_over_29'];?>" class="form-control" id="validationServer03"  required>
                            <?php endif;?>                                      
                            <div class="valid-feedback">
                              Looks,Good!!
                            </div>
                            <div class="invalid-feedback">
                             Required total age max 25!.
                            </div>
                          </div>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Submit form</button>
                      </form>

                  <!--   <form action="#" method="post" novalidate>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Status Pekerjaan</label>
                            <input type="number" required>
                        </div>
                            
                    </form> -->
                    </div>
                  </div>
                </div>            
              </div>
             
              <!-- ============================================================== -->
              <!-- End PAge Content -->
              <!-- ============================================================== -->
              <!-- ============================================================== -->
              <!-- Right sidebar -->
              <!-- ============================================================== -->
              <!-- .right-sidebar -->
              <!-- ============================================================== -->
              <!-- End Right sidebar -->
              <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
  