<?php
  
  if (empty($_POST)) {
   redirect("my404");
   die;
}


  $id_rating = $_POST['id'];
  $position1 = $_POST["position"];
  $height1 = $_POST["height"];
  $age1 = $_POST["age"];
  $ovr1 = $_POST["ovr"];
  $mv1 = $_POST["mv"];
  $sal1= $_POST["sal"];
  $idplayerrole1 = $_POST["idplayerrole"];
  $in21to291 = $_POST["in21to29"];
  $u211 = $_POST["u21"];
  $up291 = $_POST["up29"];
  $budget1 = $_POST["budget"];



  
// memfilter posisi pemain sesuai yang dipilih //
// preprocessing data //
  $recomendation = $this->db->query("
    SELECT *,

    CASE 
    WHEN height < 171 THEN 1
    WHEN height <= 181 THEN 2
    WHEN height <= 191 THEN 3
    WHEN height <= 201 THEN 4
    WHEN height <= 300 THEN 5
    END AS height_id,

    CASE 
    WHEN age < 22 THEN 1
    WHEN age <= 29 THEN 2
    WHEN age <= 30 THEN 3
    WHEN age <= 40 THEN 4
    WHEN age <= 50 THEN 5
    END AS age_id,

    CASE 
    WHEN overall < 80 THEN 1
    WHEN overall <= 85 THEN 2
    WHEN overall <= 90 THEN 3
    WHEN overall <= 100 THEN 4
    END AS overalle_id,

    CASE 
    WHEN market_value < 1 THEN 1
    WHEN market_value <= 20 THEN 2
    WHEN market_value <= 50 THEN 3
    WHEN market_value <= 100 THEN 4
    WHEN market_value <= 150 THEN 5
    WHEN market_value <= 300 THEN 6
    END AS market_value_id,

    CASE 
    WHEN salaries < 1 THEN 1
    WHEN salaries <= 4 THEN 2
    WHEN salaries <= 10 THEN 3
    WHEN salaries <= 25 THEN 4
    END AS salaries_id

    FROM player 

    JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
    JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
    JOIN `league` ON `team`.`id_league` = `league`.`id_league`
    JOIN `recomendation` ON `recomendation`.`id_player` = `player`.`id_player`

    WHERE `player`.`id_position` = $position1")->result();
    ?>
<?php

// testing fitur di urutkan sesuai form
$recomendation2 = $this->db->query("
    SELECT 

    CASE 
    WHEN height < 171 THEN 1
    WHEN height <= 181 THEN 2
    WHEN height <= 191 THEN 3
    WHEN height <= 201 THEN 4
    WHEN height <= 300 THEN 5
    END AS height_id,

    CASE 
    WHEN age < 22 THEN 1
    WHEN age <= 29 THEN 2
    WHEN age <= 30 THEN 3
    WHEN age <= 40 THEN 4
    WHEN age <= 50 THEN 5
    END AS age_id,

    CASE 
    WHEN overall < 80 THEN 1
    WHEN overall <= 85 THEN 2
    WHEN overall <= 90 THEN 3
    WHEN overall <= 100 THEN 4
    END AS overalle_id,

    CASE 
    WHEN market_value < 1 THEN 1
    WHEN market_value <= 20 THEN 2
    WHEN market_value <= 50 THEN 3
    WHEN market_value <= 100 THEN 4
    WHEN market_value <= 150 THEN 5
    WHEN market_value <= 300 THEN 6
    END AS market_value_id,

    CASE 
    WHEN salaries < 1 THEN 1
    WHEN salaries <= 4 THEN 2
    WHEN salaries <= 10 THEN 3
    WHEN salaries <= 25 THEN 4
    END AS salaries_id,
    
    id_player_role,in21to29,u21,up29,uang

    FROM player 

    JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
    JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
    JOIN `league` ON `team`.`id_league` = `league`.`id_league`
    JOIN `recomendation` ON `recomendation`.`id_player` = `player`.`id_player`

    WHERE `player`.`id_position` = $position1")->result();
    ?>
  
            
                  <?php
                  $total_row = count($recomendation);

                    $user = [
                      $height1,
                      $age1,
                      $ovr1,
                      $mv1,
                      $sal1,
                      $idplayerrole1,
                      $in21to291,
                      $u211,
                      $up291,
                      $budget1
                    ];
                    $users = [$user];

                  //merubah array objek ke array assosiatif//
                  $player = json_decode(json_encode($recomendation2), true);


//////////////////////////////////////////////////////////////////////////////////////////////////////
 //////////////proses menghitung distance tiap player dihitung per fiturnya dahulu // /////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
                  $height = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_0 = pow(($player[$i]['height_id'] - $users[0][0]),2);
                    array_push($height, $jumlah_0);
                  }
                 
                  $age = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_1 = pow(($player[$i]['age_id'] - $users[0][1]),2);
                    array_push($age, $jumlah_1);
                  }

                  $overall = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_2 = pow(($player[$i]['overalle_id'] - $users[0][2]),2);
                    array_push($overall, $jumlah_2);
                  }

                  $market_value = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_3 = pow(($player[$i]['market_value_id'] - $users[0][3]),2);
                    array_push($market_value, $jumlah_3);
                  }

                  $salaries = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_4 = pow(($player[$i]['salaries_id'] - $users[0][4]),2);
                    array_push($salaries, $jumlah_4);
                  }

                  $id_player_role = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_5 = pow(($player[$i]['id_player_role'] - $users[0][5]),2);
                    array_push($id_player_role, $jumlah_5);
                  }

                  $in21to29 = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_6 = pow(($player[$i]['in21to29'] - $users[0][6]),2);
                    array_push($in21to29,  $jumlah_6);
                  }

                  $u21 = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_7 = pow(($player[$i]['u21'] - $users[0][7]),2);
                    array_push($u21,  $jumlah_7);
                  }

                  $up29 = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                     $jumlah_8 = pow(($player[$i]['up29'] - $users[0][8]),2);
                    array_push($up29,  $jumlah_8);
                  }

                  $uang = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                     $jumlah_9 = pow(($player[$i]['uang'] - $users[0][9]),2);
                    array_push($uang,  $jumlah_9);
                  }


///////////////////////////////////////////////////////////////////////////////////////////////////////
 //////////////proses menghitung distance tiap player setelah dihitung per fiturnya // /////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

              $similarity = [];
              for ($a=0; $a < count($player) ; $a++) { 
                 $total_distance = [];
                  for ($b=0; $b < count($player) ; $b++) { 
                   $total = $height[$b] + $age[$b] + $overall[$b] + $market_value[$b] + $salaries[$b] + $id_player_role[$b] + $in21to29[$b] + $u21[$b] + $up29[$b] + $uang[$b];
                   array_push($total_distance,  $total);
                  } 

/////////////////////////////////////////////////////////////////////////////////////////////////////
 //////////////       proses menghitung bobot similarity distance     ///////////// /////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////// 
              var_dump(expression)
              $bobot = 1/(1+$total_distance[$a]);
              array_push($similarity, $bobot);
              } 

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
 //////////////   memasukkan bobot kedalam array pemain dari database  ///////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

                $z = 0; 
                foreach($recomendation as $row){ 
                $sort[$row->id_player] = $similarity; // array yang berisi data tiap id_player yang punya nilai bobot   
                $z++;
                }       

  ///////////////////////////////// HASIL REKOMENDASI ////////////////////////////////////
    // tampilkan hasil rekomendasi berdasarkan bobot terbesar ke terkecil //

        echo arsort($sort);
                   
        ?>
                <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 style = "color:black"class="page-title">Home</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Recomendation page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <!-- <div class="card">
                            <div class="card-body"> -->
                                           
                        <center class="m-b-20">
                          <h1 style="color:black" class="card-title m-t-10">Recomendation Player</h1>
                            <h5 class="card-title m-t-10">Master League PES 2020</h5>
                              <h12 class="card-title m-t-10">- Last Transfer Januari 2020 -</h12><br><br>
                                
                                <a href= "<?=base_url('user/recomendation');?>"><button class="btn btn-primary ">Try Again ?</button></a>
                        </center>

                        <button class="btn btn-primary ">#1</button>
                    </div> 

              <!-- ambil fitur data player dari database berdasarkan data player yang telah direkomendasikan dan diurutkan bobot tertinggi -->
              <?php
              $inc = 0;
                foreach ($sort as $key => $value): 
                  $inc++;
                  $row1 = $this->db->query("SELECT * FROM `player` JOIN `recomendation` ON `player`.`id_player` = `recomendation`. `id_player` JOIN `team` ON `player`.`id_team` = `team`.`id_team` JOIN `position` ON `player`.`id_position` = `position`.`id_position` JOIN `league` ON `team`.`id_league` = `league`.`id_league` WHERE `player`.`id_player` = '$key' limit 5")->row();
                  ?>

                    <a href="<?=base_url('league/'.$row1->league_name.'/'.$row1->team_name.'/players/'.$row1->name);?>">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="card1">
                            <div class="card1-body">
                              <center class="m-t-30"><img src="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->name).'.png');?>" alt="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->name).'.png');?>"width="60%"/>
                                    <h4 class="card-title m-t-10"><?= $row1->name;?></h4>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i><font class="font-medium">Overall<hr class="new5"><h4><?=$row1->overall;?></h4></font></a></div>
                                        <div class="col-2">
                                            <div class="click-to-top"><a href ="<?= base_url('league/'.$row1->league_name.'/'.$row1->team_name)?>"><img  src="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->team_name).'.png');?>" width="30 %"><span><?=$row1->team_name?></span>
                                            </div>
                                        </div>
                                        <div class="col-6"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">Market Value<hr class ="new5">€ <?=$row1->market_value;?> million </font></a></div>

                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                    <?php
                    if($inc == 4){
                        break;
                     }
                     // JIKA BOBOT ADALAH 0 TIDAK AKAN DITAMPILKAN
                     if($value == 0){
                        break;
                     }
                     ?>
                    <?php endforeach;?>
                    </a>
                  </div>
                </div>

              <!-- ambil data player rekomendasi no 1 -->
              <?php 
               $yaha = 0;
               foreach ($sort as $key => $value): 
                $yaha++;
                $row2 = $this->db->query("SELECT * FROM `player` JOIN `recomendation` ON `player`.`id_player` = `recomendation`. `id_player` JOIN `team` ON `player`.`id_team` = `team`.`id_team` JOIN `position` ON `player`.`id_position` = `position`.`id_position` JOIN `league` ON `team`.`id_league` = `league`.`id_league` WHERE `player`.`id_player` = '$key' limit 5")->row_array();
                if($yaha == 1){
                        break;
                     }
                ?>
                <?php endforeach;?>
               
              
<!-------------------------------------------- PEMBERIAN RATING ----------------------------------------->

              <?php if($rating['id_rating'] != $id_rating) :?>                 
                  <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Rate This Recomendation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                  
                        <div class="modal-body">                  
                        <center>
                        <form action="<?=base_url('user/rating');?>" method="post" class="needs-validation" novalidate>
                        <input type="hidden" name="id_player" id="id_player" value="<?=$row2['id_player'];?>">
                        <input type="hidden" name="id_rating" id="id_rating" value="<?=$id_rating;?>">
                        
                        <ul class="list-inline">
                          <select class="gl-star-rating" id="rate" name="rate" required>
                              <option value="">Select a rating</option>
                              <option value="5">Excellent</option>
                              <option value="4">Very Good</option>
                              <option value="3">Average</option>
                              <option value="2">Poor</option>
                              <option value="1">Terrible</option>
                          </select>
                        </ul>
                        </center>                   
                      </div>
                      <div class="modal-footer">   
                        <button type = "submit" class="btn btn-success btn-sm"><h6 class="m-b-0 font-16">Rate & Save</h6></button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Skip</button><br>
                        <p style="color: red">note: save and rate will return you to the recommendation page, if you haven't seen the detailed data of each player, please click the skip button</p>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              <?php endif ?>



      <script src="<?=base_url('assets/');?>dist/star-rating.min.js"></script>
      <script>
        var starRatingControl = new StarRating('.gl-star-rating',{
          maxStars: 5

      });
      </script>
   
    
   
                
 

  

                
              

  
