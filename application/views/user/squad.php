            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-12">
                      <?= $this->session->flashdata('message'); ?>
                        <h4 style = "color:black"class="page-title">My Squad Page</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">My Squad page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                   <!--  <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-danger text-white" target="_blank">Upgrade to Pro</a>
                        </div>
                    </div> -->
                </div>
            </div>

            <!--  ambil data umur dari tim yang dipilih -->
            <?php 
            $id_team = $myteam['id_team'];

            $totalup29 = $this->db->query("SELECT count(name) as totalup29 from `player` where id_team = $id_team and age > 29 ")->row_array();
            $total21to29 = $this->db->query("SELECT count(name) as total21to29 from `player` where id_team = $id_team and age between 21 and 29 ")->row_array();
            $totalu21 = $this->db->query("SELECT count(name) as totalu21 from `player` where id_team = $id_team and age < 21 ")->row_array();
              
            ?>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ==============================================================
            
            ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row justify-content-md-center">
                    <div class="col-lg-6 md-6 sm-12">
                        <div class="card">
                            <div class="card-header bg-dark">
                                <h2 class="text-center text-white">My Squad data</h2>
                            </div>    
                            <div class="card2-body">                                                
                              <center><img src="<?=base_url('assets/images/team/'.$myteam['team_name'].'/'.$myteam['team_name'].'.png');?>" class="img-fluid rounded-circle  mb-3" width="128" height="128"/></center>  
                              <h3 class="card-title m-t-10 text-center text-white"><?=$myteam['team_name'];?> <a class="text-white"href="<?=base_url('league/getPlayersbyTeam/'.$myteam['team_name']);?>"><i class="fas fa-info-circle fa-sm"></i></a></h3>
                               <center><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i class="fas fa-edit"></i> Edit</button>

                               <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#change"><i class="fas fa-edit"></i> Change Squad</button>

                               </center>

                               <br>
                              <hr class="new5">
                              <table class="table table-borderless">
                                <tbody>
                                  <tr>
                                    <td class="text-white"><i class="far fa-money-bill-alt text-success fa-2x"></i> Transfer Budget : <?php echo number_format($myteam['transfer_budget']);?></td>                         
                                    <td class="text-white"><i class="fas fa-hand-holding-usd text-success fa-2x m-t-2"></i> Salary Budget : <?php echo number_format($myteam['salary_budget']);?></td>                                                       
                                  </tr>                               
                                </tbody>
                              </table>

                              <table class="table">
                                <thead class="table table-hover">
                                  <tr>
                                    <th><b>Total age less than 21</b></th>
                                    <th><b>Total age betwwen 21-29</b></th>
                                    <th><b>Total age over than 29</b></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>
                                      <td class="text-white"><center><?=$totalu21['totalu21'];?> player</center></td>
                                      <td class="text-white"><center><?=$total21to29['total21to29'];?> player</center></td>
                                      <td class="text-white"><center><?=$totalup29['totalup29'];?> player</center></td>
                                    <?php else:?>
                                      <td class="text-white"><center><?=$myteam['total_age_less_21'];?> player</center></td>
                                      <td class="text-white"><center><?=$myteam['total_age_21to29'];?> player</center></td>
                                      <td class="text-white"><center><?=$myteam['total_age_over_29'];?> player</center></td>
                                    <?php endif;?>                                   
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- optional modsal -->
                    
                    <!-- optional modsal -->

                    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-dark">
                                    <h5 class="modal-title text-white" id="exampleModalLabel">Edit Data</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" class="text-white">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  <form method="post" action="<?= base_url('user/editMySquad/');?>" class="needs-validation" novalidate>
                                    <div class="form-row">
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Transfer Budget min (5M Euro)</label>
                                        <input type="number" min="5000000" class="form-control" id="transfer_budget" name="transfer_budget"placeholder="transfer_budget" value="<?=$myteam['transfer_budget'];?>" required>
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required transfer budget(min 5M Euro)!
                                        </div>
                                      </div>
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Salary Budget min (600K Euro)</label>
                                        <input type="number" min="600000" class="form-control" id="salary_budget" name="salary_budget"placeholder="salary_budget" value="<?=$myteam['salary_budget'];?>" required>
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required salary budget (min 600K Euro)!.
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-row">
                                      <div class="col-md-4 mb-3">
                                        <label for="validationServer03">Total age < 21</label>
                                        <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>
                                          <input type="number" name="total_age_less_21" max="25" value="<?=$totalu21['totalu21'];?>" class="form-control" id="validationServer03"  required>
                                        <?php else:?>
                                          <input type="number" name="total_age_less_21" max="25" value="<?=$myteam['total_age_less_21'];?>" class="form-control" id="validationServer03"  required>
                                        <?php endif;?>  
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required total age max 25!
                                        </div>
                                      </div>
                                      <div class="col-md-4 mb-3">
                                        <label for="validationServer04">Total age between 21-29</label>
                                        <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>
                                          <input type="number" name="total_age_21to29" max="25" value="<?=$total21to29['total21to29'];?>" class="form-control" id="validationServer05" required>
                                        <?php else:?>
                                          <input type="number" name="total_age_21to29" max="25" value="<?=$myteam['total_age_21to29'];?>" class="form-control" id="validationServer03"  required>
                                        <?php endif;?> 
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required total age max 25!
                                        </div> 
                                      </div>
                                      <div class="col-md-4 mb-3">
                                        <label for="validationServer05">Total age > 29</label>
                                        <?php if($myteam['total_age_less_21'] == 0 && $myteam['total_age_21to29'] == 0 && $myteam['total_age_over_29'] == 0):?>
                                          <input type="number" name="total_age_over_29" max="25" value="<?=$totalup29['totalup29'];?>" class="form-control" id="validationServer05" required>
                                        <?php else:?>
                                          <input type="number" name="total_age_over_29"  max="25" value="<?=$myteam['total_age_over_29'];?>" class="form-control" id="validationServer03"  required>
                                        <?php endif;?>                                      
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required total age max 25!.
                                        </div>
                                      </div>
                                    </div>
                                    <button class="btn btn-primary btn-block" type="submit">Submit form</button>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- // change team modal // -->

                     <div class="modal fade" id="change" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-dark">
                                    <h5 class="modal-title text-white" id="change">Change Team</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" class="text-white">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  <h5>This change will delete your previously data</h5>
                                  <h5>Are you sure want to change squad team ?</h5>
                                </div>
                                <div class="modal-footer">
                                  <a href="<?=base_url('user/deleteMySquad');?>"><button type="button" class="btn btn-danger">Yes</button></a>
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
              
               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
  