            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                      <?= $this->session->flashdata('message'); ?>
                        <h4 style = "color:black"class="page-title">Recomendation Page</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Recomendation page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                   <!--  <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-danger text-white" target="_blank">Upgrade to Pro</a>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ==============================================================
            
            ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row justify-content-md-center">
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                              
                                <!-- buat id rating -->
                                <?php $random = rand(1,100).substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 3);?>
                                <!-- buat id rating -->

                                <form action ="<?=base_url('user/recomendation/result');?>" method="post" class="needs-validation" novalidate>
                                 <input type="hidden" name="id" id="id" value="<?=$random;?>">
                                 <div class="form-group row">
                                   <label for="inputPassword3" class="col-sm-6 col-form-label">Position</label>
                                    <div class="col-sm-6">
                                     <select class="custom-select" name="position" required>
                                       <option value="">Choose Position</option>
                                       <option value="1">GK (GoalKeeper)</option>
                                       <option value="2">CB (Centre Back)</option>
                                       <option value="3">RB (Right Back)</option>
                                       <option value="4">LB (Left Back)</option>
                                       <option value="5">DMF (Defensive Midfielder)</option>
                                       <option value="6">CMF (Centre Midfielder)</option>
                                       <option value="7">RMF (Right Midfielder)</option>
                                       <option value="8">LMF (Left Midfilder)</option>
                                       <option value="9">AMF (Attacking Midfielder)</option>
                                       <option value="10">RWF (Right Wing Foward)</option>
                                       <option value="11">LWF (Left Wing Foward)</option>  
                                       <option value="12">CF (Centre Foward)</option>
                                       <option value="13">SS (Second Striker)</option>
                                     </select>
                                      <div class="valid-feedback">
                                            Looks,Good!!
                                      </div>
                                      <div class="invalid-feedback">
                                           Choose Position!.
                                      </div>
                                    </div>
                                 </div>
                                 <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-6 col-form-label">Height</label>
                                    <div class="col-sm-6">
                                      <select class="custom-select" name="height" required>
                                       <option value="">Choose Height</option>
                                       <option value="1">(160-170)</option>
                                       <option value="2">(171-180)</option>
                                       <option value="3">(181-190)</option>                  
                                       <option value="4">(192-200)</option>
                                       <option value="5">(201-300)</option>          
                                     </select>
                                     <div class="valid-feedback">
                                            Looks,Good!!
                                      </div>
                                      <div class="invalid-feedback">
                                           Choose Height!.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-6 col-form-label">Age</label>
                                    <div class="col-sm-6">
                                      <select class="custom-select" name="age" required>
                                       <option value="">Choose Age</option>
                                       <option value="1">(16-22)</option>
                                       <option value="2">(23-29)</option>
                                       <option value="3">(30-40)</option>                  
                                       <option value="4">(41-50)</option>
                                     </select>
                                     <div class="valid-feedback">
                                            Looks,Good!!
                                      </div>
                                      <div class="invalid-feedback">
                                           Choose Age!.
                                      </div>
                                    </div>
                                  </div>                     
                                  <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-6 col-form-label">Overall</label>
                                    <div class="col-sm-6">
                                     <select class="custom-select" name = "ovr" required>
                                       <option value="">Choose Overall</option>
                                       <option value="1">Low (70-79)</option>
                                       <option value="2">Medium (80-85)</option>
                                       <option value="3">High (86-90)</option>                           
                                       <option value="4">Very High (91-100)</option>     
                                     </select>
                                     <div class="valid-feedback">
                                            Looks,Good!!
                                      </div>
                                      <div class="invalid-feedback">
                                           Choose Overall!.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-6 col-form-label">Market Value</label>
                                    <div class="col-sm-6">
                                     <select class="custom-select" name="mv" required>
                                       <option value="">Choose Market Value</option>
                                       <option value="1">0-999k</option>
                                       <option value="2">1m-20m</option>
                                       <option value="3">21m-50m</option>                           
                                       <option value="4">51m-100m</option>     
                                       <option value="5">100m-150m</option>                           
                                       <option value="6">151m-300m</option>     
                                     </select>
                                     <div class="valid-feedback">
                                            Looks,Good!!
                                      </div>
                                      <div class="invalid-feedback">
                                           Choose Market Value!.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-6 col-form-label">Salaries</label>
                                    <div class="col-sm-6">
                                     <select class="custom-select" name="sal" required>
                                       <option value="">Choose Salaries</option>
                                       <option value="1">1-999k</option>
                                       <option value="2">1m-4m</option>
                                       <option value="3">21m-50m</option>                           
                                       <option value="4">5m-10m</option>     
                                       <option value="5">11m-25m</option>                           
                                     </select>
                                     <div class="valid-feedback">
                                            Looks,Good!!
                                      </div>
                                      <div class="invalid-feedback">
                                           Choose Salaries!.
                                      </div>
                                    
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-6 col-form-label">Player Role</label>
                                    <div class="col-sm-6">
                                     <select class="custom-select" name="idplayerrole" required>
                                       <option value="">Choose Player Role</option>
                                       <option value="1">non</option>
                                       <option value="2">leader</option>
                                       <option value="3">team player</option>                           
                                       <option value="4">risk taker</option>     
                                       <option value="5">workhorse</option>                           
                                       <option value="6">playmaker</option>
                                       <option value="7">smartplayer</option>
                                       <option value="8">bandiera</option>                           
                                       <option value="9">legend</option>     
                                       <option value="10">youth prospect</option> 
                                     </select>
                                     <div class="valid-feedback">
                                            Looks,Good!!
                                      </div>
                                      <div class="invalid-feedback">
                                           Choose Player Role!.
                                      </div>
                                    </div>
                                  </div>
                                 
                                  <fieldset class="form-group">
                                    <div class="row">
                                      <legend class="col-form-label col-sm-6 pt-0">The total of players on your team that have age between 21-29 </legend>
                                      <div class="col-sm-6">
                                        <div class="custom-control custom-radio">
                                          <input class="custom-control-input" type="radio" name="in21to29" id="ain21to29" value="1" checked>
                                          <label class="custom-control-label" for="ain21to29">
                                            More than equals 13 player
                                          </label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                          <input class="custom-control-input" type="radio" name="in21to29" id="bin21to29" value="2">
                                          <label class="custom-control-label" for="bin21to29">
                                            Less than equals 13 player
                                          </label>
                                        </div>
                                        
                                      </div>
                                    </div>
                                  </fieldset>
                                   <fieldset class="form-group">
                                    <div class="row">
                                      <legend class="col-form-label col-sm-6 pt-0">The total of players on your team who are under 21  </legend>
                                      <div class="col-sm-6">
                                        <div class="custom-control custom-radio">
                                          <input class="custom-control-input" type="radio" name="u21" id="au21" value="1" checked>
                                          <label class="custom-control-label" for="au21">
                                            More than equals 3 player
                                          </label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                          <input class="custom-control-input" type="radio" name="u21" id="bu21" value="2">
                                          <label class="custom-control-label" for="bu21">
                                            Less than equals 3 player
                                          </label>
                                        </div>
                                        
                                      </div>
                                    </div>
                                  </fieldset>
                                  <fieldset class="form-group">
                                    <div class="row">
                                      <legend class="col-form-label col-sm-6 pt-0">The total of players on your team that have age over 29 </legend>
                                      <div class="col-sm-6">
                                        <div class="custom-control custom-radio">
                                          <input class="custom-control-input" type="radio" name="up29" id="aup29" value="1" checked>
                                          <label class="custom-control-label" for="aup29">
                                            More than equals 7 player
                                          </label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                          <input class="custom-control-input" type="radio" name="up29" id="bup29" value="2">
                                          <label class="custom-control-label" for="bup29">
                                            Less than equals 7 player
                                          </label>
                                        </div>
                                        
                                      </div>
                                    </div>
                                  </fieldset>
                                   <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-6 col-form-label">Team Budget</label>
                                    <div class="col-sm-6">
                                     <select class="custom-select" name="budget" required>
                                       <option value="">Choose Budget</option>
                                       <option value="1">5jt & 600k- 6jt-800k</option>
                                       <option value="2">5jt & 600k - 17jt & 2,5 jt</option>
                                       <option value="3">5jt & 600k - 22 jt & 3,5jt</option>                           
                                       <option value="4">5jt & 600k  - 30 jt & 5 jt</option>     
                                       <option value="5">5jt & 600k- 35jt & 6jt</option>                           
                                       <option value="6">5jt & 600k - 63jt & 11 jt</option>
                                       <option value="7">harus berkembang</option>
                                     </select>
                                     <div class="valid-feedback">
                                            Looks,Good!!
                                      </div>
                                      <div class="invalid-feedback">
                                          Choose Budget and salaries!.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-sm-12">
                                      <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="mdi mdi-search-web"></i> Search Player</button>

                                    </div>
                                  </div>
                                </form>
                               <!--  <?php var_dump($_POST);?> -->

                            </div>
                        </div>
                    </div>
                </div>
               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
  