<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 style ="color:black"class="page-title">Home</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Search Page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row justify-content-md-center">
                    <div class="col-12">
        <center class="m-b-20">
            
                <h5 class="card-title m-t-10">Search Result</h5>
                    
                
            </center>
            </div>
           
              <?php $total_row = count($player);?>
              <?php $total_row2 = count($team);?>
                <?php if ($total_row != 0 && $total_row2 == 0) : ?>
                    <?php foreach ($player as $p): ?>
                    <a href="<?=base_url('league/'.$p->league_name.'/'.$p->team_name.'/players/'.$p->name);?>">
                    <div class="col-6 col-md-3 ">
                        <div class="card1">
                            <div class="card1-body">
                              <center class="m-t-30"><img src="<?= base_url('assets/images/team/'.$p->team_name.'/'.urldecode($p->name).'.png');?>" width="60%"/>
                                    <h4 class="card1-title m-t-10"><?= $p->name;?></h4>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i><font class="font-medium">Overall<hr class="new5"><h4><?=$p->overall;?></h4></font></a></div>
                                        <div class="col-2">
                                            <div class="click-to-top"><a href ="<?= base_url('league/'.$p->league_name.'/'.$p->team_name)?>"><img  src="<?= base_url('assets/images/team/'.$p->team_name.'/'.urldecode($p->team_name).'.png');?>" width="30 %"><span><?=$p->team_name?></span>
                                            </div>
                                        </div>
                                        <div class="col-6"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">Market Value<hr class="new5">€ <?=$p->market_value;?> million </font></a></div>

                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                    </a>
                <?php elseif ($total_row2 != 0 && $total_row == 0) :?>
                    <?php foreach ($team as $l): ?>
                    <div class="col-4 col-sm-2">
                        <div class="card1">
                            <div class="card1-body">
                              <center class="m-t-30"> <a href="<?=base_url('league/'.$l->league_name.'/'.$l->team_name);?>"><img src="<?= base_url('assets/images/team/'.$l->team_name.'/'.$l->img_team);?>" width="60%" />
                                    <h6 style="color:white"class="card-title m-t-10"><?= $l->team_name;?></h4></a>
                                </center>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                <?php else :?>   
                    <h5 class="card-title m-t-10">Not found...</h5> 
                <?php endif ?>
                </div>
                    <!-- <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> <img src="../../assets/images/users/5.jpg" class="rounded-circle" width="150" />
                                    <h4 class="card-title m-t-10"><?= $user['name'];?></h4>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                                    </div>
                                </center>
                            </div>
                            <div>
                        </div>
                    </div> -->
                
               
               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

    
           
            