
<style>
body{
  font-family:'Raleway', sans-serif;
  font-size :14px;
  font-weight: 400;
  color: #777;
}
h1{
  font-size:36px;
  color:#555;
  font-weight: bold;
}
h3{
  font-size:24px;
  color:#333;
  font-weight: bold;
}
#team img{
  margin-top: -50px;

}
#team i{
  font-size:2px;
  color:#555;
}
#team p{
  font-weight: 500;

}
#team .card{
  border-radius:0;
  transition: all 0.3s ease-in;
  -webkit-transition:all 0.3s ease-in;
  -moz-transition: all 0.3 ease-in;
}
#team .card:hover{
  background: #7360ee;
  color:#fff;
  border-radius:5px;
  border:none;
  box-shadow: 5px 5px 10px #9E9E9E;
  
}
#team .card:hover h3, #team .card:hover i{
  color:#fff; 
}

</style>
<?php
  
  if (empty($_POST)) {
   redirect("my404");
   die;
}

  $id_rating = $_POST['id'];
  $position1 = $_POST["position"];



  if($u211 = $_POST["total_age_less_21"] >= 3){
    $u211 = 1;
  }else{
    $u211 = 2;
  }
  
  if($in21to291 = $_POST["total_age_21to29"] >= 13){
    $in21to291 = 1;
  }else{
    $in21to291 = 2;
  }

  if($up291 = $_POST["total_age_over_29"] >= 7){
    $up291 = 1;
  }else{
    $up291 = 2;
  }
  


// budget //
  $salary = $_POST["salary_budget"];
  $budget1 = $_POST["transfer_budget"];

  if($budget1 >= 5000000 && $budget1 < 6000000){
    $budget1 = 1;
  }
  elseif($budget1  >= 6000000 && $budget1 < 17000000 ){
    $budget1 = 2;
  }

  elseif($budget1  >= 17000000  && $budget1 < 22000000 ){
    $budget1 = 3;
  }
  elseif($budget1  >= 22000000 && $budget1 < 30000000 ){
    $budget1 = 4;
  }

  elseif($budget1  >= 3000000 && $budget1 < 35000000){
    $budget1 = 5;
  }
  elseif($budget1  >= 35000000 && $budget1 < 630000000){
    $budget1 = 6;
  }
  elseif($budget1  >= 63000000){
    $budget1 = 7;
  }
  

// position// 
  if($position1 == 1 || $position1 == 2 || $position1 == 3 || $position1 == 4 ){
    $height1 = 5;
  }
  else {
    $height1 = 3;
  }


// age //
  // jika pilihan user auto
  if($_POST['age'] == 'Auto'){
    $maks21 = 3;
    $maks21to29 = 13;
    $maksover29 = 7;

    if($u211 < $maks21){
      $_POST['age'] = 1;
    }
    elseif($in21to291 < $maks21to29){
      $_POST['age'] = 2;
    }
    elseif($up291 < $maksover29){
      $_POST['age'] = 3;
    }    
    elseif($in21to291 < $maks21to29){
      $_POST['age'] = 2;
    }
    elseif($u211 < $maks21 && $in21to291 < $maks21to29){
      $_POST['age'] = 1;
    }
    elseif($u211 < $maks21 && $up291 < $maksover29){
      $_POST['age'] = 1;
    }
    elseif($in21to291 < $maks21to29 && $up291 < $maksover29){
      $_POST['age'] = 2;
    }

  }
  else{
    $age1 = $_POST["age"];  
  }

// overall //
  $age1 = $_POST["age"];
  if($age1 == 1){
    $ovr1 = 1;
  }
  else{
    $ovr1 = 3;  
  }
  

  $mv1 = 2;
  $sal1= 2;


// player role //
  if($age1 == 1){
    $idplayerrole1 = 2;
  }
  else{
    $idplayerrole1 = 10; 
  }
  


$mysquadteam = $myteam['team_name'];
  
// memfilter posisi pemain sesuai yang dipilih //
// preprocessing data //
  $recomendation = $this->db->query("
    SELECT *,

    CASE 
    WHEN height < 171 THEN 1
    WHEN height <= 181 THEN 2
    WHEN height <= 191 THEN 3
    WHEN height <= 201 THEN 4
    WHEN height <= 300 THEN 5
    END AS height_id,

    CASE 
    WHEN age < 21 THEN 1
    WHEN age <= 29 THEN 2
    WHEN age <= 30 THEN 3
    WHEN age <= 40 THEN 4
    WHEN age <= 50 THEN 5
    END AS age_id,

    CASE 
    WHEN overall < 80 THEN 1
    WHEN overall <= 85 THEN 2
    WHEN overall <= 90 THEN 3
    WHEN overall <= 100 THEN 4
    END AS overalle_id,

    CASE 
    WHEN market_value < 1 THEN 1
    WHEN market_value <= 20 THEN 2
    WHEN market_value <= 50 THEN 3
    WHEN market_value <= 100 THEN 4
    WHEN market_value <= 150 THEN 5
    WHEN market_value <= 300 THEN 6
    END AS market_value_id,

    CASE 
    WHEN salaries < 1 THEN 1
    WHEN salaries <= 4 THEN 2
    WHEN salaries <= 10 THEN 3
    WHEN salaries <= 25 THEN 4
    END AS salaries_id

    FROM player 

    JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
    JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
    JOIN `league` ON `team`.`id_league` = `league`.`id_league`
    JOIN `recomendation` ON `recomendation`.`id_player` = `player`.`id_player`

    WHERE `player`.`id_position` = $position1 and `team`.`team_name` != '$mysquadteam' ")->result();
   
   
                  $total_row = count($recomendation);

                    $user = [
                      $height1,
                      $age1,
                      $ovr1,
                      $mv1,
                      $sal1,
                      $idplayerrole1,
                      $in21to291,
                      $u211,
                      $up291,
                      $budget1
                    ];
                    $users = [$user];

                

                  //merubah array objek ke array assosiatif//
                  $player = json_decode(json_encode($recomendation), true);


//////////////////////////////////////////////////////////////////////////////////////////////////////
 //////////////proses menghitung distance tiap player dihitung per fiturnya dahulu 
 ////////////////////////////// tahap rumus  tiap fitur |dataset|-|data input|  // /////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
                  $height = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_0 = abs($player[$i]['height_id'] - $users[0][0]);
                    array_push($height, $jumlah_0);
                  }
                 
                  $age = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_1 = abs($player[$i]['age_id'] - $users[0][1]);
                    array_push($age, $jumlah_1);
                  }

                  $overall = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_2 = abs($player[$i]['overalle_id'] - $users[0][2]);
                    array_push($overall, $jumlah_2);
                  }

                  $market_value = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_3 = abs($player[$i]['market_value_id'] - $users[0][3]);
                    array_push($market_value, $jumlah_3);
                  }

                  $salaries = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_4 = abs($player[$i]['salaries_id'] - $users[0][4]);
                    array_push($salaries, $jumlah_4);
                  }

                  $id_player_role = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_5 = abs($player[$i]['id_player_role'] - $users[0][5]);
                    array_push($id_player_role, $jumlah_5);
                  }

                  $in21to29 = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_6 = abs($player[$i]['in21to29'] - $users[0][6]);
                    array_push($in21to29,  $jumlah_6);
                  }

                  $u21 = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                    $jumlah_7 = abs($player[$i]['u21'] - $users[0][7]);
                    array_push($u21,  $jumlah_7);
                  }

                  $up29 = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                     $jumlah_8 = abs($player[$i]['up29'] - $users[0][8]);
                    array_push($up29,  $jumlah_8);
                  }

                  $uang = [];
                   for ($i=0; $i < count($player) ; $i++) { 
                     $jumlah_9 = abs($player[$i]['uang'] - $users[0][9]);
                    array_push($uang,  $jumlah_9);
                  }


///////////////////////////////////////////////////////////////////////////////////////////////////////
 //////////////proses menghitung jumlah distance tiap player setelah dihitung per fiturnya // /////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

              
              
                 // $total_distance = [];
                 //  for ($b=0; $b < count($player) ; $b++) { 
                 //   $total = $height[$b] + $age[$b] + $overall[$b] + $market_value[$b] + $salaries[$b] + $id_player_role[$b] + $in21to29[$b] + $u21[$b] + $up29[$b] + $uang[$b];
                 //   array_push($total_distance,  $total);
                 //  } 

    

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
 //////////////   memasukkan bobot kedalam array pemain dari database  ///////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

                $b = 0; 
                foreach($recomendation as $row){ 
                $total = $height[$b] + $age[$b] + $overall[$b] + $market_value[$b] + $salaries[$b] + $id_player_role[$b] + $in21to29[$b] + $u21[$b] + $up29[$b] + $uang[$b];
                $sort[$row->id_player] = $total; // array yang berisi data tiap id_player yang punya nilai bobot   
                $b++;
                }       

  ///////////////////////////////// HASIL REKOMENDASI ////////////////////////////////////
    // tampilkan hasil rekomendasi berdasarkan bobot terkecil ke  terbesar //

        asort($sort);
        
    
                   
        ?>
                <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 style = "color:black"class="page-title">Home</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Recomendation page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <section id="team">
                  <div class="row">                  
                    <div class="col-lg-8 sm-12"> 
                      <div class="card">
                        <div class="card-header bg-dark">
                            <h3 class="card-title text-white">Your Choice</h1>
                        </div>
                        <div class="card2-body">
                         
                          <table class="table">
                            <tbody>                        
                              <tr>
                                <td class="text-white"><b>Position</b></th>
                                <td class="text-white">
                                  <?php foreach ($myposition as $m ): ?>   
                                    <?php if ($m->id_position == $position1): ?>

                                            <?php if ($m->position == '1'):?>
                                            <button class="btn btn-dark btn-sm"><h4 class="m-b-0 font-16" style="color: orange;"><?=$m->position;?></h4>

                                            <?php elseif ($m->position == '2'|| $m->position == '3' ||$m->position == '4' ):?>
                                            <button class="btn btn-dark btn-sm"><h4 class="m-b-0 font-16" style="color: #0099cc;"><?=$m->position;?></h4> 

                                            <?php elseif ($m->id_position == '5'|| $m->position == '6' ||$m->position == '7'|| $m->position == '8' || $m->position == '9' ):?>
                                            <button class="btn btn-dark btn-sm"><h4 class="m-b-0 font-16" style="color:#00cc00;"><?=$m->position;?></h4>

                                            <?php else :?>
                                           <button class="btn btn-dark btn-sm"><h4 class="m-b-0 font-16" style="color:red;"><?=$m->position;?></h4> 
                                            <?php endif ?>      

                                     
                                    <?php endif;?>  
                                  <?php endforeach?>                              
                                </td>
                              </tr>
                              <tr>
                                <td class="text-white"><b>Height</b></th>
                                <td class="text-white">
                                  <?php $heights= ['','160-170','171-180','181-190','191-200','201-300'];
                                  echo '~ '.$heights[$height1];?>
                                </td>                          
                              </tr>
                              <tr>
                                <td class="text-white"><b>Age</b></th>
                                <td class="text-white">
                                  <?php $ages= ['','16-21','22-29','30-40','41-50'];
                                  echo '~ '.$ages[$age1];?>
                                </td>                     
                              </tr>
                              <tr>
                                <td class="text-white"><b>Market Value</b></th>
                                <td class="text-white">
                                  <?php $market_value = ['','0-999k','1m-20m','21-50m','51-100m','101-150m','151-300m'];
                                  echo '~ '.$market_value[$mv1];?>
                                </td>
                               
                              </tr>
                              <tr>
                                <td class="text-white"><b>Salary</b></th>
                                <td class="text-white">
                                  <?php $salariess= ['','1k-999k','1m-4m','5m-10m','11m-25m'];
                                  echo '~ '.$salariess[$sal1];?>
                                </td>                          
                              </tr>
                              <tr>
                                <td class="text-white"><b>Player Role</b></th>
                                <td class="text-white">
                                  <?php foreach ($player_roles as $p) {
                                    if ($p->id_player_role == $idplayerrole1) {
                                      echo '~ '.$p->player_role;
                                    }
                                  }
                                  ?> 
                                  
                                </td>                          
                              </tr>
                              <tr>
                                <td class="text-white"><b>Overall</b></th>
                                <td class="text-white">
                                  <?php $overalls= ['','70-79','80-85','86-90','91-100'];
                                  echo '~ '.$overalls[$ovr1];?>
                                </td>                          
                              </tr>
                              

                            </tbody>
                          </table>
                        </div>
                      </div>                                    
                      
                      <br>                           
                    </div>
                    <div class="col-lg-4 sm-12">
                      <div class="card">
                        <div class="card-header bg-dark">
                          
                        </div>                  
                        <div class="card2-body">
                         <center><img src="<?=base_url('assets/images/team/'.$myteam['team_name'].'/'.$myteam['team_name'].'.png');?>"class="img-fluid rounded-circle w-60 mb-3"/></center>               
                            <h3 class="card-title m-t-10 text-center text-white"><?=$myteam['team_name'];?></h1>
                              
                            <table class="table">
                              <tbody>
                                <tr>
                                  <td class="text-white"><b>Transfer Budget</b></th>
                                  <td class="text-white">€ <?= number_format($_POST['transfer_budget']);?></td>
                                </tr>
                                <tr>
                                  <td class="text-white"><b>Salary Budget</b></th>
                                  <td class="text-white">€ <?= number_format($_POST['salary_budget']);?></td>
                                </tr>
                                <tr>
                                  <td class="text-white"><b>Total age less than 21</b></th>
                                  <td class="text-white"><?=$_POST['total_age_less_21'];?> player                         
                                </tr>
                                <tr>
                                 <td class="text-white"><b>Total age betwwen 21-29</b></th>
                                 <td class="text-white"><?=$_POST['total_age_21to29'];?> player                         
                                </tr>
                                <tr>
                                  <td class="text-white"><b>Total age over than 29</b></th>
                                  <td class="text-white"><?=$_POST['total_age_over_29'];?> player                         
                                </tr>
                              </tbody>
                            </table>                     
                        </div>
                      </div>                                         
                    </div>
                  </div>
                </section>
                <div class="row justify-content-md-center">
                  <div class="col-lg-6 sm-12">                 
                    <center class="m-b-20">
                      <div class="card">
                        <div class="card-header bg-dark">
                          <h4 style="text-white">Note</h4>
                        </div>
                        <div class="card2-body">
                          <blockquote class="blockquote mb-0">
                            <p class="text-white">Your Squad Condition</p>

                            <?php if($u211 == 2) : ?>
                              <footer class="blockquote-footer text-white">Need more player that have age under 20</footer>                       
                            <?php endif;?>

                            <?php if($in21to291 == 2) : ?>
                              <footer class="blockquote-footer text-white">Need more player that have age between 21 to 29</footer>                       
                            <?php endif;?>

                            <?php if($up291 == 2) : ?>
                              <footer class="blockquote-footer text-white">Need more player that have age over 29</footer>                       
                            <?php endif;?>

                            <?php if($idplayerrole1 == 2) : ?>
                              <footer class="blockquote-footer text-white">Because your team need more young player, We recomend to buy player with "Youth prospect" player role</footer>                       
                            <?php endif;?>

                            <?php if($height1 == 5) : ?>
                              <footer class="blockquote-footer text-white">Because your choose GK position player, We recomend to buy a tallest player </footer>                    
                            <?php endif;?>
                          </blockquote>
                        </div>
                      </div>       
                    </center>           
                  </div>
                </div>
                <div class="row">
                  <div class = col-lg-12>             
                    <center class="m-b-20">
                      <h1 style="color:black" class="card-title m-t-10">Our Recomendation</h1>
                        <h5 class="card-title m-t-10">Master League PES 2020</h5>
                          <h12 class="card-title m-t-10">- Last Transfer Januari 2020 -</h12><br><br>
                            
                            <a href= "<?=base_url('user/recomendation');?>"><button class="btn btn-primary ">Try Again ?</button></a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                              Please Rate This Recomendation
                            </button>

                    </center>
                    <button class="btn btn-primary ">#1</button>
                  </div> 
                

              <!-- ambil fitur data player dari database berdasarkan data player yang telah direkomendasikan dan diurutkan bobot tertinggi -->
              <?php
              $inc = 0;
                foreach ($sort as $key => $value): 
                  $inc++;
                  $row1 = $this->db->query("SELECT * FROM `player` JOIN `recomendation` ON `player`.`id_player` = `recomendation`. `id_player` JOIN `team` ON `player`.`id_team` = `team`.`id_team` JOIN `position` ON `player`.`id_position` = `position`.`id_position` JOIN `league` ON `team`.`id_league` = `league`.`id_league` WHERE `player`.`id_player` = '$key' limit 5")->row();
                  ?>

                    <a href="<?=base_url('league/'.$row1->league_name.'/'.$row1->team_name.'/players/'.$row1->id_player);?>">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="card1">
                            <div class="card1-body">
                              <center class="m-t-30"><img src="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->name).'.png');?>" alt="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->name).'.png');?>"width="60%"/>
                                    <h4 class="card-title m-t-10"><?= $row1->name;?></h4>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i><font class="font-medium">Overall<hr class="new5"><h4><?=$row1->overall;?></h4></font></a></div>
                                        <div class="col-2">
                                            <div class="click-to-top"><a href ="<?= base_url('league/'.$row1->league_name.'/'.$row1->team_name)?>"><img  src="<?= base_url('assets/images/team/'.$row1->team_name.'/'.urldecode($row1->team_name).'.png');?>" width="30 %"><span><?=$row1->team_name?></span>
                                            </div>
                                        </div>
                                        <div class="col-6"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">Market Value<hr class ="new5">€ <?=$row1->market_value;?> million </font></a></div>

                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                    <?php
                    if($inc == 4){
                        break;
                     }
                     // JIKA BOBOT ADALAH 0 TIDAK AKAN DITAMPILKAN
                     if($value == 0){
                        break;
                     }
                     ?>
                    <?php endforeach;?>
                    </a>
                  </div>
                </div>

              <!-- ambil data player rekomendasi no 1 -->
              <?php 
               $yaha = 0;
               foreach ($sort as $key => $value): 
                $yaha++;
                $row2 = $this->db->query("SELECT * FROM `player` JOIN `recomendation` ON `player`.`id_player` = `recomendation`. `id_player` JOIN `team` ON `player`.`id_team` = `team`.`id_team` JOIN `position` ON `player`.`id_position` = `position`.`id_position` JOIN `league` ON `team`.`id_league` = `league`.`id_league` WHERE `player`.`id_player` = '$key' limit 5")->row_array();
                if($yaha == 1){
                        break;
                     }
                ?>
                <?php endforeach;?>
               
              
<!-------------------------------------------- PEMBERIAN RATING ----------------------------------------->

              <?php if($rating['id_rating'] != $id_rating) :?>                 
                  <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Rate This Recomendation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                  
                        <div class="modal-body">                  
                        <center>
                        <form action="<?=base_url('user/rating');?>" method="post" class="needs-validation" novalidate>
                        <input type="hidden" name="id_player" id="id_player" value="<?=$row2['id_player'];?>">
                       <!--  <input type="hidden" name="id_rating" id="id_rating" value="<?=$id_rating;?>"> -->
                        
                        <ul class="list-inline">
                          <select class="gl-star-rating" id="rate" name="rate" required>
                              <option value="">Select a rating</option>
                              <option value="4">Excellent</option>
                              <option value="3">Very Good</option>
                              <!-- <option value="3">Average</option> -->
                              <option value="2">Poor</option>
                              <option value="1">Terrible</option>
                          </select>
                        </ul>
                        </center>                   
                      </div>
                      <div class="modal-footer">   
                        <button type = "submit" class="btn btn-success btn-sm"><h6 class="m-b-0 font-16">Rate & Save</h6></button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Skip</button><br>
                        <p style="color: red">note: save and rate will return you to the recommendation page, if you haven't seen the detailed data of each player, please click the skip button</p>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
              <?php endif ?>



      <script src="<?=base_url('assets/');?>dist/star-rating.min.js"></script>
      <script>
        var starRatingControl = new StarRating('.gl-star-rating',{
          maxStars: 5

      });
      </script>
   
    
   
                
 

  

                
              

  
