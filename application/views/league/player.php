<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h4 style = "color:black"class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                   <li class="breadcrumb-item"><a href="<?=base_url('user');?>">Home</a></li>
                                   <li class="breadcrumb-item"><a href="<?=base_url('league');?>">League Page</a></li>
                                    <li class="breadcrumb-item "><a href="<?=base_url('league/'.$player['league_name']);?>"><?=$player['league_name'];?></a></li>
                                    <li class="breadcrumb-item"><a href="<?=base_url('league/'.$player['league_name'].'/'.$player['team_name']);?>"><?=$player['team_name'];?></a></li>
                                    <li class="breadcrumb-item active "><?=$player['name'];?></a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Table -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    
                </div>
                <!-- ============================================================== -->
                <!-- Table -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
               <div class="row justify-content-md-center ">
                    <!-- column -->
                   
                    <div class="col-lg-5">
                        <div class="card1">
                            <div class="card1-body">
                                <div class="display-5 text-info"><a href="javascript:history.go(-1)"><i class="fas fa-arrow-circle-left"></i></a></div>
                                <center>
                                <!-- <div class="d-flex align-items-center flex-row m-t-10 "> -->
                                    <div class="display-5 text-info"><a class = "galpop-single" href="<?= base_url("assets/images/team/".$player['team_name'].'/'.urldecode($player['name'])).'.'.'png';?>"><img src="<?= base_url("assets/images/team/".$player['team_name'].'/'.urldecode($player['name'])).'.'.'png';?>" ></a></div>

                                    <div class="m-l-0">
                                        <h3 style="color:white"class="m-b-0"><?=$player['name'];?> 
                                        <?php if($this->session->userdata('role_id') == 1 ):?>
                                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit"><i class="fas fa-edit"></i> Edit</button>
                                        <?php else:?>
                                        <?php endif;?>
                                    </h3>
                                       <small style="color:white">Master League PES 2020</small>
                                    </div>
                                <!-- </div> -->
                                </center>
                                <table class="table no-border mini-table m-t-20 text-center">
                                    <tbody>
                                        <tr>
                                            <td class="text-muted">Team</td>
                                            <td class="font-medium"><a href="<?=base_url('league/'.$player['league_name'].'/'.$player['team_name']);?>"><img width ="30px" src="<?= base_url("assets/images/team/".$player['team_name'].'/'.urldecode($player['team_name'])).'.'.'png';?>" ></a> <?=$player['team_name'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-muted">Position</td>
                                            <?php if ($player['id_position'] == '1'):?>
                                            <td class="font-medium"><button class="btn btn-dark btn-sm"><h4 class="m-b-0 font-16" style="color: orange;"><?=$player['position'];?></h4></td> 

                                            <?php elseif ($player['id_position'] == '2'|| $player['id_position'] == '3' ||$player['id_position'] == '4' ):?>
                                            <td class="font-medium"><button class="btn btn-dark btn-sm"><h4 class="m-b-0 font-16" style="color: #0099cc;"><?=$player['position'];?></h4></td> 

                                            <?php elseif ($player['id_position'] == '5'|| $player['id_position'] == '6' ||$player['id_position'] == '7'|| $player['id_position'] == '8' || $player['id_position'] == '9' ):?>
                                            <td class="font-medium"><button class="btn btn-dark btn-sm"><h4 class="m-b-0 font-16" style="color:#00cc00;"><?=$player['position'];?></h4></td> 

                                            <?php else :?>
                                            <td class="font-medium"><button class="btn btn-dark btn-sm"><h4 class="m-b-0 font-16" style="color:red;"><?=$player['position'];?></h4></td> 
                                            <?php endif ?>      
                                        </tr>
                                        <tr>
                                            <td class="text-muted">Height</td>
                                            <td class="font-medium"><?=$player['height'];?> cm</td>
                                        </tr>
                                        <tr>
                                            <td class="text-muted">Age</td>
                                            <td class="font-medium"><?=$player['age'];?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-muted">Player Role</td>
                                            <td class="font-medium"><span1 style="text-align:left"title="Basic Effect:<br><?=$player['basic_effect'];?><br>Special Effect: <?=$player['special_effect'];?>"><?=$player['player_role'];?></span1>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                <ul class="row list-style-none text-center m-t-30">
                                    <li class="col-4">
                                        <h1 class="text-success"><i class="far fa-money-bill-alt"></i></h1>
                                        <span style="color:white" class="d-block text-muted">Market Value</span>
                                        <h6  style="color:white"class="m-t-5">€ <?=$player['market_value'];?> Million</h6>
                                    </li>
                                    <li class="col-4">
                                        <h1 class="text-success"><i class="fas fa-hand-holding-usd"></i></h1>
                                        <span style="color:white" class="d-block text-muted">Salaries</span>
                                        <h6  style="color:white" class="m-t-5">€ <?=$player['salaries'];?> Million/Year</h6>
                                    </li>
                                    <li class="col-4">
                                        <h1 class="text-success"><i class="fas fa-star"></i></i></h1>
                                        <span style="color:white" class="d-block text-muted">Overall</span>
                                        <h6 style="color:white" class="m-t-5"><?=$player['overall'];?></h6>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- edit modaal -->
                <?php 
                  $team = $this->db->get('team')->result();
                  $player_role = $this->db->get('player_role')->result();
                ?>
                    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-dark">
                                    <h5 class="modal-title text-white" id="exampleModalLabel">Edit Data</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" class="text-white">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  <h2 style="color:black">Player data</h2>
                                  <form method="post" action="<?= base_url('user/editMyPlayer/');?>" class="needs-validation" novalidate>
                                    <div class="form-row">
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Name</label>
                                        <input type="text"class="form-control" id="name" name="name"placeholder="Player Name" value="<?=$player['name'];?>" required>
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required transfer budget(min 5M Euro)!
                                        </div>
                                      </div>
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Team Name</label>
                                        <select  class="form-control" id="team_name" name="team_name" required>
                                          <?php foreach ($team as $t ):?>
                                            <?php if ($t->id_team == $player['id_team']):?>
                                            <option value = "<?= $t->id_team; ?>"selected><?=$t->team_name;?></option>      
                                            <?php else:?>
                                              <option value = "<?= $t->id_team; ?>"><?=$t->team_name;?></option>      
                                            <?php endif;?>                                 
                                          <?php endforeach; ?>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-row">
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Height</label>
                                        <input type="number" min="600000" class="form-control" id="height" name="height"placeholder="height" value="<?=$player['height'];?>" required>
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required salary budget (min 600K Euro)!.
                                        </div>
                                      </div>
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Age</label>
                                        <input type="number" min="600000" class="form-control" id="age" name="age"placeholder="salary_budget" value="<?=$player['age'];?>" required>
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required salary budget (min 600K Euro)!.
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-row">
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Overall</label>
                                        <input type="number" min="600000" class="form-control" id="overall" name="overall"placeholder="overall" value="<?=$player['overall'];?>" required>
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required salary budget (min 600K Euro)!.
                                        </div>
                                      </div>
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Market Value</label>
                                        <input type="number" min="600000" class="form-control" id="market_value" name="market_value"placeholder="market_value" value="<?=$player['market_value'];?>" required>
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required salary budget (min 600K Euro)!.
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-row">
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Salaries</label>
                                        <input type="number" min="600000" class="form-control" id="salaries" name="salaries"placeholder="salary_budget" value="<?=$player['salaries'];?>" required>
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required salary budget (min 600K Euro)!.
                                        </div>
                                      </div>
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer01">Player Role</label>
                                        <select class="form-control" name="id_player_role" id="id_player_role" required>
                                          <?php foreach ($player_role as $p) :?>
                                            <?php if($p->id_player_role == $player['id_player_role']):?>
                                              <option value="<?=$p->id_player_role;?>"selected><?=$p->player_role;?></option>
                                            <?php else :?>
                                              <option value="<?=$p->id_player_role;?>"><?=$p->player_role;?></option>
                                            <?php endif;?>
                                          <?php endforeach;?> 
                                        </select>
                                      </div>
                                    </div>
                                    <button class="btn btn-block btn-primary" type="submit">Edit</button>
                                  </form>
                                    <hr>
                                    <h2 style="color:black">For Squad Data</h2><br>
                                    <div class="form-row">
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer05">Total < 21</label>
                                          <input type="number" name="up29" max="25" value="<?=$player['u21'];?>" class="form-control" id="validationServer03"  required>                               
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required total age max 25!
                                        </div>
                                      </div>
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer03">Total age between 21-29</label>
                                          <input type="number" name="in21to29" max="25" value="<?=$player['in21to29'];?>" class="form-control" id="validationServer03"  required>                               
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required total age max 25!
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-row">
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer03">Total age > 29</label>
                                          <input type="number" name="up29" max="25" value="<?=$player['up29'];?>" class="form-control" id="validationServer03"  required>                               
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required total age max 25!
                                        </div>
                                      </div>
                                      <div class="col-md-6 mb-3">
                                        <label for="validationServer03">Budget (euro)</label>
                                          <input type="number" name="up29" max="25" value="<?=$player['uang'];?>" class="form-control" id="validationServer03"  required>                               
                                        <div class="valid-feedback">
                                          Looks,Good!!
                                        </div>
                                        <div class="invalid-feedback">
                                         Required total age max 25!
                                        </div>
                                      </div>
                                    </div>
                                    <button class="btn btn-primary btn-block" type="submit">Submit form</button>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->