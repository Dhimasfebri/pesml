 
            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">League Page</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=base_url('user');?>">Home</a></li>
                                    <li class="breadcrumb-item"><a href="<?=base_url('league');?>">League Page</a></li>
                                    <!-- <?php foreach ($league as $l): ?>
                                    <li class="breadcrumb-item active" aria-current="page"><?=$l->league_name;?></li>
                                    <?php endforeach;?> --> 
                                </ol>
                            </nav>
                        </div>
                    </div>
                   <!--  <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-danger text-white" target="_blank">Upgrade to Pro</a>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ==============================================================
            <============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center flex-row">
                                    <div class="display-5 text-info">
                                        
                                        <a href="<?= base_url("league/leagueid/".$league['league_name']); ?>"><img src="<?= base_url("assets/images/league/".$league['img_league']);?>" width="100px"></a></div>
                                    <div class="m-l-10">
                                        <h3 class="m-b-0"><?=$league['league_name'];?></h3><small><?=$league['ket'];?></small>
                                    </div>
                                       
                                    </div>
                                </div>
                                <!-- title -->
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title m-t-10"></h4>               
                                    </div>
                                <!-- title -->
                            </div>
                            <div class="table-responsive">
                                <table class="table v-middle">
                                    <thead>
                                        <tr class="bg-light">
                                            <th class="border-top-0">#</th>
                                            <th class="border-top-0">Team Name</th>
                                        </tr>
                                    </thead>
                                    <?php $i = 1;?>
                                    <?php foreach ($league2 as $l): ?>
                                    <tbody>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="m-r-10"><a href="<?=base_url('league/'.$l->league_name.'/'.$l->team_name);?>" class="btn btn-circle btn-info text-white"><?=$l->team_name;?></a></div>
                                                    <div class="">
                                                        <h4 class="m-b-0 font-16"><?=$l->team_name;?></h4>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                    <?php $i ++; ?>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
  