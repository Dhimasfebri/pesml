 
            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h4 style="color:black" class="page-title">League Page</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    
                                    <li class="breadcrumb-item"><a href="<?=base_url('user');?>">Home</a></li>
                                    <li class="breadcrumb-item"><a href="<?=base_url('league');?>">League Page</a></li>
                                    <li class="breadcrumb-item "><a href="<?=base_url('league/'.$players['league_name']);?>"><?=$players['league_name'];?></a></li>
                                    <li class="breadcrumb-item active "><?=$players['team_name'];?></a></li>
                                    
                                </ol>
                            </nav>
                        </div>
                    </div>
                   <!--  <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-danger text-white" target="_blank">Upgrade to Pro</a>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ==============================================================
            <============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card2-body">
                                <div class="d-flex align-items-center flex-row">
                                    <div class="display-5 text-info">
                                        
                                       <a href="#"><img src="<?= base_url("assets/images/team/".$players['team_name'].'/'.$players['img_team']);?>" width="100px"></a></div>
                                    <div class="m-l-10">
                                        <h3 style="color:white" class="m-b-0"><?=$players['team_name'];?></h3><small style="color:white"><?=$players['ket'];?></small><img src="<?= base_url('assets/images/flag_ITA.png');?>" width = '5%'>
                                        <br>
                                            <small style="color:white">Number of players:  <b><?=$total['total'];?><i>( Only player with Overall 70++)</i></small>
                                        <br>
                                            <small style="color:white">The most valueable player: <b><a  href="<?=base_url('league/'.$players['league_name'].'/'.$players['team_name'].'/players/'.$value['name']);?>"><font style="color:white"><?=$value['name'];?> | <?=$value['market_value'];?> million euro</b></font></small></a>
                                    </div>
                                        
                                   
                                </div>
                                <!-- title -->
                                <div class="ml-auto m-t-20">
                                    <div class="dl">
                                        <div class="input-group">
                                            <span class="input-group-prepend">
                                                <div class="input-group-text bg-transparent border-right-0">
                                                    <i style="color:white" class="fa fa-search"></i>
                                                </div>
                                            </span>
                                            <input class="form-control py-2 border-left-0 border" id="myInput" onkeyup="myFunction()" placeholder="Search & filter..."type="text" id="example-search-input" />
                                            
                                        </div>
                                    </div>
                                </div>

                                
                            <!-- title -->
                            </div>
                            <div class="table-responsive">
                                <table class="table v-middle" id="myTable">
                                    <thead>
                                        <tr class="bg-light">
                                            <th class="border-top-0">#</th>
                                            <th class="border-top-0"><a class="column_sort" id="id_position" data-order="desc" data-team = "<?=$players['team_name'];?>"href="#">Position <span class="fa fa-arrow-down"></span></th>
                                            <th class="border-top-0"><a class="column_sort" id="name" data-team = "<?=$players['team_name'];?>" data-order="desc" href="#">Name</th>
                                            <th class="border-top-0"><a class="column_sort" id="overall" data-team = "<?=$players['team_name'];?>" data-order="desc" href="#">Overall</th>
                                            <th class="border-top-0"><a class="column_sort" id="market_value" data-team = "<?=$players['team_name'];?>" data-order="desc" href="#">Market Value</th>
                                            <th class="border-top-0"><a class="column_sort" id="salaries" data-team = "<?=$players['team_name'];?>" data-order="desc" href="#">Salaries</th>

                                            <?php if ($this->session->userdata('role_id') == 1) :?>
                                            <th class="border-top-0">Action</th>
                                            <?php else :?>
                                            <?php endif;?>
                                        </tr>
                                    </thead>
                                    
                                    <?php foreach ($player as $p): ?>
                                    <tbody>
                                        <tr>
                                            <td><a href="<?=base_url('league/'.$p->league_name.'/'.$p->team_name.'/players/'.$p->id_player);?>">
                                                <div class="click-to-top1">
                                                    <img src="<?= base_url("assets/images/team/".$p->team_name.'/'.urldecode($p->name)).'.'.'png';?>" ></td></a>
                                                </div>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="m-t-20">
                                                        <?php if ($p->id_position == '1'):?>
                                                            <button class="btn btn-dark btn-sm"><h1 class="m-b-0 font-16" style="color: orange;"><?=$p->position;?></h1></button>
                                                        <?php elseif ($p->id_position == '2' || $p->id_position == '3' || $p->id_position == '4' ):?>
                                                            <button class="btn btn-dark btn-sm"><h6 class="m-b-0 font-16" style="color: #0099cc;"><?=$p->position;?></h6></button>
                                                        <?php elseif ($p->id_position == '5'|| $p->id_position == '6' || $p->id_position == '7' || $p->id_position == '8' || $p->id_position == '9'):
                                                            ?>
                                                            <button class="btn btn-dark btn-sm"><h6 class="m-b-0 font-16" style="color:#00cc00;"><?=$p->position;?></h6></button>
                                                        <?php elseif ($p->id_position == '10' || $p->id_position == '11' || $p->id_position == '12'|| $p->id_position == '13'):?>
                                                            <button class="btn btn-dark btn-sm"><h1 class="m-b-0 font-16" style="color:red;"><?=$p->position;?></h1></button>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-t-20">
                                                    <a href="<?=base_url('league/'.$p->league_name.'/'.$p->team_name.'/players/'.$p->id_player);?>"><?=$p->name;?></td>
                                                    </a>
                                                </div>
                                            <td>
                                                <div class="m-t-15">
                                                    <?php if ($p->overall < '85'):?>
                                                    <button class="btn btn-warning btn-sm"><h6 class="m-b-0 font-16"><?=$p->overall;?></h6></button>
                                                    <?php else :?>
                                                    <button class="btn btn-success btn-sm"><h6 class="m-b-0 font-16"><?=$p->overall;?></h6></button>
                                                    <?php endif ?>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="m-t-20">
                                                    € <?=$p->market_value;?> Million 
                                                 </div>
                                            </td>
                                            <td>
                                                <div class="m-t-20">
                                                    € <?=$p->salaries;?> Million / year
                                                </div>
                                            </td>

                                            <?php if ($this->session->userdata('role_id') == 1) :?>
                                            <td>
                                                <div class="m-t-20">
                                                    
                                                    <a href="javascipt:void(0)" data-toggle="modal"   data-target="#editModal<?= $p->id_player ?>" class="btn btn-circle btn-success btn-sm"><i class="fas fa-fw fa-pencil-alt"></i></a>

                                                    <a href="<?= base_url("#"); ?>"data-toggle="tooltip" data-placement="top" title="hapus" class="btn btn-circle btn-danger btn-sm tombol-hapus"><i class="fas fa-fw fa-trash"></i></a>
                                                </div>
                                            </td>
                                            <?php else :?>
                                            <?php endif;?>
                                            

                                        </tr>
                                        <tr>
                                            <!-- edit modaal -->
                                        <?php 
                                          $team = $this->db->get('team')->result();
                                          $player_role = $this->db->get('player_role')->result();
                                        ?>
                                        <div class="modal fade" id="editModal<?= $p->id_player ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-dark">
                                                        <h5 class="modal-title text-white" id="exampleModalLabel">Edit Data</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true" class="text-white">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                      <h2 style="color:black">Player data</h2>
                                                      <form method="post" action="<?= base_url('user/editMyPlayer/');?>" class="needs-validation" novalidate>
                                                        <div class="form-row">
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer01">Name</label>
                                                            <input type="text"class="form-control" id="name" name="name"placeholder="Player Name" value="<?=$p->name;?>" required>
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required transfer budget(min 5M Euro)!
                                                            </div>
                                                          </div>
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer01">Team Name</label>
                                                            <select  class="form-control" id="team_name" name="team_name" required>
                                                              <?php foreach ($team as $t ):?>
                                                                <?php if ($t->id_team == $p->id_team):?>
                                                                <option value = "<?= $t->id_team; ?>"selected><?=$t->team_name;?></option>      
                                                                <?php else:?>
                                                                  <option value = "<?= $t->id_team; ?>"><?=$t->team_name;?></option>      
                                                                <?php endif;?>                                 
                                                              <?php endforeach; ?>
                                                            </select>
                                                          </div>
                                                        </div>
                                                        <div class="form-row">
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer01">Height</label>
                                                            <input type="number" min="600000" class="form-control" id="height" name="height"placeholder="height" value="<?=$p->height;?>" required>
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required salary budget (min 600K Euro)!.
                                                            </div>
                                                          </div>
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer01">Age</label>
                                                            <input type="number" min="600000" class="form-control" id="age" name="age"placeholder="salary_budget" value="<?=$p->age;?>" required>
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required salary budget (min 600K Euro)!.
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="form-row">
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer01">Overall</label>
                                                            <input type="number" min="600000" class="form-control" id="overall" name="overall"placeholder="overall" value="<?=$p->overall;?>" required>
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required salary budget (min 600K Euro)!.
                                                            </div>
                                                          </div>
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer01">Market Value</label>
                                                            <input type="number" min="600000" class="form-control" id="market_value" name="market_value"placeholder="market_value" value="<?=$p->market_value;?>" required>
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required salary budget (min 600K Euro)!.
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="form-row">
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer01">Salaries</label>
                                                            <input type="number" min="600000" class="form-control" id="salaries" name="salaries"placeholder="salary_budget" value="<?=$p->salaries;?>" required>
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required salary budget (min 600K Euro)!.
                                                            </div>
                                                          </div>
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer01">Player Role</label>
                                                            <select class="form-control" name="id_player_role" id="id_player_role" required>
                                                              <?php foreach ($player_role as $pr) :?>
                                                                <?php if($pr->id_player_role == $p->id_player_role):?>
                                                                  <option value="<?=$p->id_player_role;?>"selected><?=$pr->player_role;?></option>
                                                                <?php else :?>
                                                                  <option value="<?=$p->id_player_role;?>"><?=$pr->player_role;?></option>
                                                                <?php endif;?>
                                                              <?php endforeach;?> 
                                                            </select>
                                                          </div>
                                                        </div>
                                                        <button class="btn btn-block btn-primary" type="submit">Edit</button>
                                                      </form>
                                                        <hr>
                                                        <h2 style="color:black">For Squad Data</h2><br>
                                                        <form method="post" action="<?= base_url('user/editMyPlayer/');?>" class="needs-validation" novalidate>
                                                        <div class="form-row">
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer05">Total < 21</label>
                                                              <input type="number" name="up29" max="25" value="<?=$p->u21;?>" class="form-control" id="validationServer03"  required>                               
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required total age max 25!
                                                            </div>
                                                          </div>
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer03">Total age between 21-29</label>
                                                              <input type="number" name="in21to29" max="25" value="<?=$p->in21to29;?>" class="form-control" id="validationServer03"  required>                               
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required total age max 25!
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="form-row">
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer03">Total age > 29</label>
                                                              <input type="number" name="up29" max="25" value="<?=$p->up29;?>" class="form-control" id="validationServer03"  required>                               
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required total age max 25!
                                                            </div>
                                                          </div>
                                                          <div class="col-md-6 mb-3">
                                                            <label for="validationServer03">Budget (euro)</label>
                                                              <input type="number" name="up29" max="25" value="<?=$players['uang'];?>" class="form-control" id="validationServer03"  required>                               
                                                            <div class="valid-feedback">
                                                              Looks,Good!!
                                                            </div>
                                                            <div class="invalid-feedback">
                                                             Required total age max 25!
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <button class="btn btn-primary btn-block" type="submit">Submit form</button>
                                                      </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                  
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
  <script>
       $(document).ready(function(){
          $(document).on('click', '.column_sort', function(){
             var nama_kolom = $(this).attr("id");
             var order = $(this).data("order");
             var team = $(this).data("team");
             var arrow = '';
             if(order == 'desc'){
                  arrow = '&nbsp;<span class="fa fa-arrow-up"></span>';
             } else {
                  arrow = '&nbsp;<span class="fa fa-arrow-down"></span>';
             }
             $.ajax({
                url:"<?=base_url('user/jhghgkujgkjhgkhjgkjhgkjhgkjhgkhjgkjhgkyugkjgykgygky');?>",
                method:"POST",
                data:{nama_kolom:nama_kolom, order:order, team:team},
                success:function(data)
                {
                     $('#myTable').html(data);
                     $('#'+nama_kolom+'').append(arrow);
                }
             })
          });
       });
    </script>