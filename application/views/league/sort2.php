<?php
  
  $output = '';
  $order = $_POST["order"];
  if($order == 'desc'){
      $order = 'asc';
  } else {
    $order = 'desc';
  }
 
  $nama_kolom = $_POST["nama_kolom"];
  $orderby = $_POST["order"];
  $team = $_POST["team"];

 
  $query = $this->db->query("SELECT * FROM `player` 
      JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
      JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
      JOIN `league` ON `team`.`id_league` = `league`.`id_league`
      WHERE TEAM.team_name ='".$team."' ORDER BY `player`.". $nama_kolom ." ". $orderby ." ");
  $res1 = $query->result();
  


  // $query = "SELECT * FROM player ORDER BY player.". $nama_kolom ." ". $orderby ."";
  // $dewan1 = $db1->prepare($query);
  // $dewan1->execute();
  // $res1 = $dewan1->get_result();
 ?>     
 
        <div class="table-responsive">
            <table class="table v-middle" id="myTable">
                <thead>
                    <tr class="bg-light">
                        <th class="border-top-0">#</th>
                        <th class="border-top-0"><a class="column_sort" id="id_position" data-team ="<?=$team;?>"  data-order="<?=$order;?>" href="#">Position</th>
                        <th class="border-top-0"><a class="column_sort" id="name" data-team ="<?=$team;?>"  data-order="<?=$order;?>" href="#">Name</th>
                        <th class="border-top-0"><a class="column_sort" id="overall" data-team ="<?=$team;?>"  data-order="<?=$order;?>" href="#">Overall</th>
                        <th class="border-top-0"><a class="column_sort" id="market_value" data-team ="<?=$team;?>"  data-order="<?=$order;?>" href="#">Market Value</th>
                        <th class="border-top-0"><a class="column_sort" id="salaries" data-team ="<?=$team;?>"  data-order="<?=$order;?>" href="#">Salaries</th>
                       <?php if ($this->session->userdata('role_id') == 1) :?>
                        <th class="border-top-0">Action</th>
                       <?php else :?>
                       <?php endif;?>
                    </tr>
                </thead>
                
                <?php foreach ($res1 as $p): ?>
                <tbody>
                    <tr>
                        <td><a href="<?=base_url('league/'.$p->league_name.'/'.$p->team_name.'/players/'.$p->name);?>">
                            <div class="click-to-top1">
                                <img src="<?= base_url("assets/images/team/".$p->team_name.'/'.urldecode($p->name)).'.'.'png';?>" ></td></a>
                            </div>
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="m-t-20">
                                    <?php if ($p->id_position == '1'):?>
                                        <button class="btn btn-dark btn-sm"><h1 class="m-b-0 font-16" style="color: orange;"><?=$p->position;?></h1></button>
                                    <?php elseif ($p->id_position == '2' || $p->id_position == '3' || $p->id_position == '4' ):?>
                                        <button class="btn btn-dark btn-sm"><h6 class="m-b-0 font-16" style="color: #0099cc;"><?=$p->position;?></h6></button>
                                    <?php elseif ($p->id_position == '5'|| $p->id_position == '6' || $p->id_position == '7' || $p->id_position == '8' || $p->id_position == '9'):
                                        ?>
                                        <button class="btn btn-dark btn-sm"><h6 class="m-b-0 font-16" style="color:#00cc00;"><?=$p->position;?></h6></button>
                                    <?php elseif ($p->id_position == '8'|| $p->id_position == '9' || $p->id_position == '10' || $p->id_position == '11' || $p->id_position == '12'|| $p->id_position == '13'):?>
                                        <button class="btn btn-dark btn-sm"><h1 class="m-b-0 font-16" style="color:red;"><?=$p->position;?></h1></button>
                                    <?php endif ?>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="m-t-20">
                                <a href="<?=base_url('league/'.$p->league_name.'/'.$p->team_name.'/players/'.$p->name);?>"><?=$p->name;?></td>
                                </a>
                            </div>
                        <td>
                            <div class="m-t-15">
                                <?php if ($p->overall < '85'):?>
                                <button class="btn btn-warning btn-sm"><h6 class="m-b-0 font-16"><?=$p->overall;?></h6></button>
                                <?php else :?>
                                <button class="btn btn-success btn-sm"><h6 class="m-b-0 font-16"><?=$p->overall;?></h6></button>
                                <?php endif ?>
                            </div>
                        </td>
                        <td>
                            <div class="m-t-20">
                                € <?=$p->market_value;?> Million 
                            </div>
                        </td>
                        <td>
                            <div class="m-t-20">
                                € <?=$p->salaries;?> Million / year
                            </div>
                        </td>

                        <?php if ($this->session->userdata('role_id') == 1) :?>
                          <td>
                            <div class="m-t-20">
                              <a href="<?= base_url("#"); ?>" data-toggle="tooltip" data-placement="top" title="edit" class="btn btn-circle btn-success btn-sm"><i class="fas fa-fw fa-pencil-alt"></i></a>
                              <a href="<?= base_url("#"); ?>"data-toggle="tooltip" data-placement="top" title="hapus" class="btn btn-circle btn-danger btn-sm tombol-hapus"><i class="fas fa-fw fa-trash"></i></a>
                            </div>
                          </td>
                          <?php else :?>
                          <?php endif;?>

                    </tr>
                    <tr>
                
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
