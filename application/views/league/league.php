 
            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-12">
                        <h4 style ="color:black"class="page-title">League Page</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">League Page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                   <!--  <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-danger text-white" target="_blank">Upgrade to Pro</a>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ==============================================================
            <============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card2-body">
                                <div class="d-flex align-items-center flex-row">
                                    <div class="display-5 text-info">
                                        
                                        <a href="<?= base_url("league/".$league['league_name']); ?>"><img src="<?= base_url("assets/images/league/".$league['img_league']);?>" width="100px"></a></div>
                                    <div class="m-l-10">
                                        <h3 style="color:white"class="m-b-0"><?=$league['league_name'];?></h3><small style="color:white"><?=$league['ket'];?><img src="<?= base_url('assets/images/flag_ITA.png');?>" width = '10%'></small>
                                            <br>
                                            <small style="color:white">Number of teams:  20 teams</small>
                                    </div>
                                       
                                    </div>
                                </div>
                                <!-- title -->
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title m-t-10"></h4>               
                                    </div>

                                <!-- title -->
                            </div>
                        </div>
                    </div>
                    <?php foreach ($league2 as $l): ?>
                    <div class="col-lg-2 col-md-3 col-6">
                        <div class="card1">
                            <div class="card1-body">
                              <center class="m-t-30"> <a href="<?=base_url('league/'.$l->league_name.'/'.$l->team_name);?>"><img src="<?= base_url('assets/images/team/'.$l->team_name.'/'.$l->img_team);?>" width="60%" />
                                    <h6 style="color:white"class="card-title m-t-10"><?= $l->team_name;?></h4></a>
                                </center>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
  