
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?=base_url('assets/');?>images/bg-pes.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-30">
					<img src="<?= base_url('assets/');?>images/icons/pes.png" width="50" height="40" alt="homepage" class="light-logo" />
					<img src="<?= base_url('assets/');?>images/text2.png" width="50%" alt="homepage" class="light-logo" />
				</span>
				<!-- <div class="wrap-login100 p-t-10 p-b-50">
					<?= $this->session->flashdata('message'); ?>
				</div>
 -->
				<?= $this->session->flashdata('message'); ?>
				<form method='post' action= '<?= base_url('auth/forgot_password'); ?>' class="login100-form validate-form p-b-33 p-t-3">

					<div class="wrap-input100 validate-input" data-validate = "Enter email">
						<input class="input100" type="email" name="email" placeholder="Email" 
						value ="<?= set_value('email'); ?>">	
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
						<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>
					<br>
					<div class="container-login100-form-btn m-t-10">
						<button type ="submit" class="login100-form-btn">
							Reset your password
						</button>
					</div><br>
					<center><a href="<?= base_url('auth'); ?>">Back to login</a></center>
				</form>
				
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
