<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Registration Page</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../assets/images/icons/pes.png">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css"href="../assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/main.css">

<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('../assets/images/bg-reg.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					<img src="<?= base_url('assets/');?>images/icons/pes.png" width="50" height="40" alt="homepage" class="light-logo" />
					<img src="<?= base_url('assets/');?>images/text2.png" alt="homepage" width="50%" class="light-logo" />
				</span>
				<form class="login100-form validate-form p-b-33 p-t-5" method="post" action="<?= base_url('auth/registration'); ?>">
					<div class="wrap-input100 validate-input" data-validate = "Enter Username">
						<input class="input100" type="text" name="name" placeholder="Username" value ="<?=set_value('name'); ?>">	
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Enter Email">
						<input class="input100" type="email" name="email" placeholder="Email" value ="<?=set_value('email'); ?>">	
						<span class="focus-input100" data-placeholder="&#xe86b;"></span>
						<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass1" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
						<?= form_error('pass1', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Repeat password">
						<input class="input100" type="password" name="pass2" placeholder="Repeat Password">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>

					</div>
					<div class="container-login100-form-btn m-t-32">
						<button type="submit" class="login100-form-btn">
							Register
						</button>
					</div>
					<br>
					<center><p>Already sign up?</p><a href="<?= base_url('auth'); ?>"><b>click here</a></b></center>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	<!--===============================================================================================-->
	<script src="../assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../assets/vendor/bootstrap/js/popper.js"></script>
	<script src="../assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="../assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../assets/js/main.js"></script>

</body>
</html>
	
