
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?=base_url('assets/');?>images/bg-pes.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-30">
					<img src="<?= base_url('assets/');?>images/icons/pes.png" width="50" height="40" alt="homepage" class="light-logo" />
					<img src="<?= base_url('assets/');?>images/text2.png" width="50%" alt="homepage" class="light-logo" />
				</span>
				<!-- <div class="wrap-login100 p-t-10 p-b-50">
					<?= $this->session->flashdata('message'); ?>
				</div>
 -->
				<?= $this->session->flashdata('message'); ?>
				<form method='post' action= "<?= base_url('auth/changepassword'); ?>" class="login100-form validate-form p-b-33 p-t-3">

					<div class="wrap-input100 validate-input" data-validate = "Enter password">
						<input class="input100" type="password" name="password1" placeholder="Enter new password" 
						value ="">	
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
						<?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Enter password">
						<input class="input100" type="password" name="password2" placeholder="Repeat new password" 
						value ="">	
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
						<?= form_error('password2', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>
					<br>
					<div class="container-login100-form-btn m-t-10">
						<button type ="submit" class="login100-form-btn">
							Change your password
						</button>
					</div><br>
					<center>for email : <?=$this->session->userdata('reset_email');?></center><br>
					
				</form>
				
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
