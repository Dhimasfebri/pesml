  <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 style = "color:black"class="page-title">Management League</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">League</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-danger text-white" target="_blank">Add new data</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
              
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Table -->
                <!-- ============================================================== -->
                    <!-- column -->
                    
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- title -->
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 style = "color:black"class="card-title">League data</h4>
                                        
                                    </div>
                                </div>
                                <!-- title -->
                           
                            
                                <div class="table-responsive">
                                   <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th >#</th>
                                                <th >League</th>
                                                <th >Name</th>
                                                <th >Ket</th>
                                                <th >Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1;?>
                                            <?php foreach ($league as $u ) :?>
                                            <tr>
                                                <td><?=$i;?></td>
                                                <td class= "m-t-20"><img class="bg-dark" src="<?= base_url('assets/images/league/'.$u->img_league);?>" width="60px" /></a></td>
                                                <td><?=$u->league_name;?></td>
                                                <td><?=$u->ket;?></td>
                                                <td><center>
                                                    <a href="<?= base_url("#"); ?>"data-toggle="tooltip" data-placement="top" title="detail" class="btn btn-circle btn-info btn-sm"><i class="fas fa-fw fa-info-circle"></i></a>
                                                    <a href="<?= base_url("#"); ?>" data-toggle="tooltip" data-placement="top" title="edit" class="btn btn-circle btn-success btn-sm"><i class="fas fa-fw fa-pencil-alt"></i></a>
                                                    <a href="<?= base_url("#"); ?>"data-toggle="tooltip" data-placement="top" title="hapus" class="btn btn-circle btn-danger btn-sm tombol-hapus"><i class="fas fa-fw fa-trash"></i></a>
                                                </center>
                                                </td>
                                            </tr>
                                            <?php $i++;?>
                                            <?php endforeach ;?>                                   
                                        </tbody>
                                    </table>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Table -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
     