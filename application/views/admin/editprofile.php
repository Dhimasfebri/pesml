
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="page-breadcrumb">
    <div class="row align-items-center">
      <div class="col-5">
        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
        <?= $this->session->flashdata('message'); ?>
        <h4 style = "color:black"class="page-title">Profile Page</h4>
        <div class="d-flex align-items-center">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?=base_url('admin');?>">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">My Profile</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row justify-content-md-center">
      <!-- Column -->
      <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
          <div class="card-body">
            <center class="m-t-30">
              <div class = "container">
                <img src="<?=base_url('assets/images/users/'.$user['image']);?>" class="rounded-circle image" width="10px" />
                <div class="middle">
                  <div class="text">
                    <a href="#" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-camera fa-4x"></i>Change Picture</div>
                    </a>
                  </div>
                </div>
                <h4 class="card-subtitle m-t-20"><b>- ADMIN -</h4></b>
                <h4 class="card-subtitle m-t-10"><b><?= $user['name'];?></h4></b>
            </center>
          </div>  
          <hr>
          <div class="card-body"> <small class="text-muted">Email address </small>
            <h6><?= $user['email'];?> </h6> 
            <small class="text-muted p-t-30 db">Member Since</small>
            <h6><?= date('d F Y', $user['date_created']);?>  </h6>
            <br>
            <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#exampleModalCenter2" >Edit profile</button>
          </div>
        </div>
      </div>
    </div>
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Edit Profile</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row justify-content-md-center">
              <div class="col-lg-10 col-xlg-6 col-md-7">
                <div class="card">
                  <div class="card-body">
                    <form class="form-horizontal form-material" action="<?=base_url('admin/editprofile');?>" method="post" class="needs-validation" novalidate>
                      <input type="hidden" name="id" id="id" value="<?= $user['id_user'];?>">

                      <div class="form-group">
                        <label for="email" class="col-md-12">Email</label>
                        <div class="col-md-12">
                          <input type="email" placeholder="Email" class="form-control form-control-line" value="<?= $user['email']; ?>" id="email" name="email" readonly><?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                          <div class="valid-feedback">
                            Looks,Good!!
                          </div>
                          <div class="invalid-feedback">
                            Insert Email!
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-md-12">Full Name</label>
                        <div class="col-md-12">
                          <input type="text" placeholder="Full name" class="form-control form-control-line" value="<?= $user['name']; ?>" id="name" name="name" required><?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
                          <div class="valid-feedback">
                            Looks,Good!!
                          </div>
                          <div class="invalid-feedback">
                            Insert Name!
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-12">
                          <button type = "submit" class="btn btn-success">Update Profile</button>
                        </div>
                      </div>
                    </form><hr>
                    <form action = "<?=base_url('admin/editpass');?>" method="post" class="needs-validation" novalidate>
                      <div class="form-group">                                      
                        <label for="pass1" class="col-md-12">Current Password</label>
                        <div class="col-md-12">
                          <input type="password" placeholder="" class="form-control form-control-line" id="current_password" name="current_password" required><?= form_error('current_password', '<small class="text-danger pl-3">', '</small>'); ?>
                          <small id="passwordHelpInline" class="text-muted">
                            min length 3 character
                          </small>
                          <div class="invalid-feedback">
                            Insert Password!
                          </div>
                        </div>
                      </div>
                      <div class="form-group">                                      
                        <label for="new_password1" class="col-md-12">New Password</label>
                        <div class="col-md-12">
                          <input type="password" placeholder="" class="form-control form-control-line" id="new_password1" name="new_password1" required><?= form_error('new_password1', '<small class="text-danger pl-3">', '</small>'); ?>
                          <small id="passwordHelpInline" class="text-muted">
                            min length 3 character
                          </small>
                          <div class="invalid-feedback">
                            Insert Password!
                          </div>
                        </div>
                      </div>            
                      <div class="form-group">                                      
                        <label for="new_password2" class="col-md-12">Repeat Password</label>
                        <div class="col-md-12">
                          <input type="password" placeholder="" class="form-control form-control-line" id="new_password2" name="new_password2" required><?= form_error('new_password2', '<small class="text-danger pl-3">', '</small>'); ?>
                          <small id="passwordHelpInline" class="text-muted">
                            min length 3 character
                          </small>
                          <div class="invalid-feedback">
                            Insert Password!
                          </div>
                        </div>
                      </div>         
                      <div class="form-group">
                        <div class="col-sm-12">
                          <button type = "submit" class="btn btn-success">Change password</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
</div>

   <!-- Modal -->
   <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= base_url('admin/editpp'); ?>" enctype="multipart/form-data" method="post" class="needs-validation" novalidate> 
            <center><img src="<?=base_url('assets/images/users/'.$user['image']);?>" class="rounded-circle" width="25%"></center><br>
            <div class="form-group row">
              <label for="berkas" class="col-sm-4 col-form-label">Upload image </label>
              <div class="col-12">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="image" name="image" required>
                  <small id="passwordHelpInline" class="text-muted">
                    File JPG|PNG|GIF
                  </small>
                  <div class="valid-feedback">
                    Looks,Good!!
                  </div>
                  <div class="invalid-feedback">
                    Upload Your Picture!
                  </div>
                  <label class="custom-file-label" for="berkas">Upload image</label>
                </div>
              </div>
            </div>          

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>




