  <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 style = "color:black"class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
              
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Table -->
                <!-- ============================================================== -->
                 <div class="row">
                    <!-- column -->
                    <div class="col-lg-3 col-sm-6 col-6">
                        <div class="card">
                            <div class="card-body">
                                <!-- title -->
                                 <div class="row no-gutters align-items-center">
                                  <div class="col m-r-2">
                                    <a href="#" class="btn">
                                      <i style="opacity: 0.3" class ="fas fa-users fa-3x text-info"></i>
                                    </a>
                                  </div>
                                  <div class="col-auto">
                                    <div class="d-md-flex align-items-center">
                                         <h4 style = "color:black"class="card-title">Total user</h4>
                                    </div>
                                    <div class="d-md-flex align-items-center">
                                         <h5 class="card-title"><?= $total; ?></h5>
                                    </div>
                                    
                                  </div>
                                </div>
                                <!-- title -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-6">
                        <div class="card">
                            <div class="card-body">
                                <!-- title -->
                                 <div class="row no-gutters align-items-center">
                                  <div class="col m-r-2">
                                    <a href="#" class="btn">
                                      <i style="opacity: 0.3" class ="far fa-futbol fa-3x text-secondary"></i>
                                    </a>
                                  </div>
                                  <div class="col-auto">
                                    <div class="d-md-flex align-items-center">
                                         <h4 style = "color:black"class="card-title">Total league</h4>
                                    </div>
                                    <div class="d-md-flex align-items-center">
                                         <h5 class="card-title"><?= $total3; ?></h5>
                                    </div>
                                    
                                  </div>
                                </div>

                                <!-- title -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-6">
                        <div class="card">
                            <div class="card-body">
                                <!-- title -->
                                <!-- <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 style = "color:black"class="card-title">Total Player</h4>
                                        <h5 class="card-title"><?=$total4;?></h4>
                                    </div>
                                </div> -->
                                <div class="row no-gutters align-items-center">
                                  <div class="col m-r-2">
                                    <a href="#" class="btn">
                                      <i style="opacity: 0.3" class ="fas fa-hand-rock fa-3x text-success"></i>
                                    </a>
                                  </div>
                                  <div class="col-auto">
                                    <div class="d-md-flex align-items-center">
                                         <h4 style = "color:black"class="card-title">Total player</h4>
                                    </div>
                                    <div class="d-md-flex align-items-center">
                                         <h5 class="card-title"><?= $total4; ?></h5>
                                    </div>
                                    
                                  </div>
                                </div>
                                <!-- title -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-6">
                        <div class="card">
                            <div class="card-body">
                                <!-- title -->
                                
                                <div class="row no-gutters align-items-center">
                                  <div class="col m-r-2">
                                    <a href="#" class="btn">
                                      <i style="opacity: 0.3" class ="fas fa-shield-alt fa-3x text-danger"></i>
                                    </a>
                                  </div>
                                  <div class="col-auto">
                                    <div class="d-md-flex align-items-center">
                                         <h4 style = "color:black"class="card-title">Total team</h4>
                                    </div>
                                    <div class="d-md-flex align-items-center">
                                         <h5 class="card-title"><?= $total2; ?></h5>
                                    </div>
                                    
                                  </div>
                                </div>
                                <!-- title -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- title -->
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 style = "color:black"class="card-title">User data</h4>
                                        
                                    </div>
                                </div>
                                <!-- title -->
                           
                                

                                <div class="table-responsive">
                                   <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th >Profile Image</th>
                                                <th >Name</th>
                                                <th >Email</th>
                                                <th >Date Created</th>
                                                <th >Average Rating</th>
                                                <th >Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($users as $u ) :?>
                                            <tr>
                                                <td><center>
                                                    <div class="user-pic"><img src="<?=base_url('assets/images/users/'.$u->image);?>" alt="users" class="rounded-circle" width="50" /></div>
                                                    </center>
                                                </td>
                                                <td class= "m-t-20"><?=$u->name;?></td>
                                                <td><?=$u->email;?></td>
                                                <td><center>
                                                    <label class="label label-danger"><?= date('d F Y', $user['date_created']);?></label>
                                                    </center>
                                                </td>
                                                <td>
                                                    <?php $email = $u->email;?>
                                                    <?php $avgUserRating = $this->db->query("SELECT avg(rating) as avg from `rating` where email = '$email'")->row_array();
                                                    echo $avgUserRating['avg'];
                                                    if ($avgUserRating['avg'] == null){
                                                        echo '-';
                                                    }
                                                    ?>

                                                </td>
                                                <td>
                                                    <a href="<?= base_url("#"); ?>"data-toggle="tooltip" data-placement="top" title="detail" class="btn btn-circle btn-info btn-sm"><i class="fas fa-fw fa-info-circle"></i></a>
                                                    <a href="<?= base_url("#"); ?>" data-toggle="tooltip" data-placement="top" title="edit" class="btn btn-circle btn-success btn-sm"><i class="fas fa-fw fa-pencil-alt"></i></a>
                                                    <a href="<?= base_url("#"); ?>"data-toggle="tooltip" data-placement="top" title="hapus" class="btn btn-circle btn-danger btn-sm tombol-hapus"><i class="fas fa-fw fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php endforeach ;?>                                   
                                        </tbody>
                                    </table>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Table -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Recent comment and chats -->
                <!-- ============================================================== -->
            </div>
     