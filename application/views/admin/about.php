<!DOCTYPE>

<head>
  <!-- Site made with Mobirise Website Builder v4.12.3, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.12.3, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?=base_url('assets/');?>images/icons/pes.png" type="image/x-icon">
  <meta name="description" content="">
  
  
  <title>About</title>
  <link rel="stylesheet" href="<?=base_url('new_assets/');?>web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?=base_url('new_assets/');?>bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url('new_assets/');?>bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?=base_url('new_assets/');?>bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?=base_url('new_assets/');?>socicon/css/styles.css">
  <link rel="stylesheet" href="<?=base_url('new_assets/');?>tether/tether.min.css">
  <link rel="stylesheet" href="<?=base_url('new_assets/');?>theme/css/style.css">
  <link rel="preload" as="style" href="<?=base_url('new_assets/');?>mobirise/css/mbr-additional.css">
  <link rel="stylesheet" href="<?=base_url('new_assets/');?>/mobirise/css/mbr-additional.css" type="text/css">

  <!-- <link href="<?= base_url('assets/');?>dist/css/style.min.css" rel="stylesheet"> -->
  
  
  
</head>
<body>
  <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
  <section class="header3 cid-rTXcRLwF3I mbr-fullscreen mbr-parallax-background" id="header3-i">

    

    <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(0, 0, 0);">
    </div>

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-figure" style="width: 100%;">
                <img src="<?=base_url('new_assets/');?>images/ml2-1068x601.jpg" alt="Mobirise" title="">
            </div>

            <div class="media-content">
                <h1 class="mbr-section-title mbr-white pb-3 mbr-fonts-style display-1">ABOUT</h1>
                
                <div class="mbr-section-text mbr-white pb-3 ">
                    <p class="mbr-text mbr-fonts-style display-5">PES-ML is a web application that can recommend users to buy players in PES 2020 Master League</p>
                </div>
                <div class="mbr-section-btn"><a class="btn btn-md btn-primary display-4" href="<?=base_url('admin');?>">BACK HOME<br></a></div>
            </div>
        </div>
    </div>

</section>

<section class="engine"></section><section class="carousel slide testimonials-slider cid-rTXcXU1Lnm" data-interval="false" id="testimonials-slider1-j">
    
    

    

    <div class="container text-center">
        <h2 class="pb-5 mbr-fonts-style display-2">
            MY PROFILE</h2>

        <div class="carousel slide" role="listbox" data-pause="true" data-keyboard="false" data-ride="carousel" data-interval="5000">
            <div class="carousel-inner">
                
                
            <div class="carousel-item">
                    <div class="user col-md-8">
                        <div class="user_image">
                            <img src="<?=base_url('new_assets/');?>images/dsc00747-400x267.jpg"  alt="" title="">
                        </div>
                        <div class="user_text pb-3">
                            <p class="mbr-fonts-style display-7">
                                My name is Dhimas, i'm a college student in Malang University</p>
                        </div>
                        <div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">
                            DHIMAS</div>
                        <div class="user_desk mbr-light mbr-fonts-style display-7">DEVELOPER</div>
                    </div>
                </div></div>

           <!--  <div class="carousel-controls">
                <a class="carousel-control-prev" role="button" data-slide="prev">
                  <span aria-hidden="true" class="mbri-arrow-prev mbr-iconfont"></span>
                  <span class="sr-only">Previous</span>
                </a>
                
                <a class="carousel-control-next" role="button" data-slide="next">
                  <span aria-hidden="true" class="mbri-arrow-next mbr-iconfont"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div> -->
        </div>
    </div>
</section>

<section class="cid-rTXcYWpHA1" id="footer1-k">

    

    

    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    
                        <img src="<?=base_url('assets/');?>images/text2.png" width='200px' alt="PES-ML">
                    
                </div>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3"></h5>
                <p class="mbr-text"></p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Contacts
                </h5>
                <p class="mbr-text">
                    Email: dhimasucarn5@gmail.com
                    
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3"></h5>
                <p class="mbr-text"></p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020 PES-ML
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="https://twitter.com/mobirise" target="_blank">
                                <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.facebook.com/pages/Mobirise/1616226671953247" target="_blank">
                                <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                            </a>
                        </div>
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

  <script src="<?= base_url('assets/');?>dist/js/custom.js"></script>

  <script src="<?=base_url('new_assets/');?>web/assets/jquery/jquery.min.js"></script>
  <script src="<?=base_url('new_assets/');?>popper/popper.min.js"></script>
  <script src="<?=base_url('new_assets/');?>bootstrap/js/bootstrap.min.js"></script>
  <script src="<?=base_url('new_assets/');?>tether/tether.min.js"></script>
  <script src="<?=base_url('new_assets/');?>parallax/jarallax.min.js"></script>
  <script src="<?=base_url('new_assets/');?>bootstrapcarouselswipe/bootstrap-carousel-swipe.js"></script>
  <script src="<?=base_url('new_assets/');?>mbr-testimonials-slider/mbr-testimonials-slider.js"></script>
  <script src="<?=base_url('new_assets/');?>smoothscroll/smooth-scroll.js"></script>
  <script src="<?=base_url('new_assets/');?>theme/js/script.js"></script>

 
  <script src="<?= base_url('assets/');?>dist/js/custom.js"></script>

  
</body>
</html>