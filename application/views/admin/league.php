 
            <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 style="color:black" class="page-title">Edit Team Page</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit Team Page</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                   <!--  <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-danger text-white" target="_blank">Upgrade to Pro</a>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ==============================================================
            <============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">                   
                    </div>
                    <?php foreach ($league as $l): ?>
                    <div class="col-3">
                        <div class="card1">
                            <div class="card1-body">
                              <center class="m-t-10"> 
                              <?php if($l->league_name != 'Serie-A'): ?>
                              <a href="<?= base_url('my404');?>"> 
                              <img src="<?= base_url('assets/images/league/'.$l->img_league);?>" width="100px" /></a>
                              <?php else :  ?>
                              <a href="<?= base_url('admin/league/'.$l->league_name);?>"> 
                              <img src="<?= base_url('assets/images/league/'.$l->img_league);?>" width="100px" /></a>
                                   <!--  <h4 class="card-title m-t-10"><?= $l->league_name;?></h4></a> -->
                              <?php endif?>
                              </center>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
  