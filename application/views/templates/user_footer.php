                                        
        <div class="switch">
            <div class="circle"></div>
        </div>  
                                    
        <div class="progress-wrap">
            <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
                <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"/>
            </svg>
        </div>
         <footer class="footer text-center">
                All Rights Reserved by Xtreme Admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
         </footer>

        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

  
    <script src="<?= base_url('assets/');?>libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= base_url('assets/');?>libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= base_url('assets/');?>libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url('assets/');?>dist/js/app-style-switcher.js"></script>

    <!--Wave Effects -->
    <script src="<?= base_url('assets/');?>dist/js/waves.js"></script>

    <!--Menu sidebar -->
    <script src="<?= base_url('assets/');?>dist/js/sidebarmenu.js"></script>

    <?php if($this->session->userdata('role_id') == 1 ) : ?>
      <script src="<?= base_url('assets/');?>dist/js/custom2.js"></script>
    <?php else : ?>
      <script src="<?= base_url('assets/');?>dist/js/custom.js"></script>
    <?php endif ?>
     
    <!--chartis chart-->
    <script src="<?= base_url('assets/');?>libs/chartist/dist/chartist.min.js"></script>
    <script src="<?= base_url('assets/');?>libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="<?= base_url('assets/');?>dist/js/pages/dashboards/dashboard1.js"></script>

    <!-- tooltip bubble -->
    <script src="<?= base_url('assets/');?>js/FlstBubble-min.js"></script>

    <!-- pop up gambar apik -->
    <script src="<?= base_url('assets/');?>js/jquery.galpop.min.js"></script>

    <!-- live validation -->
    <script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js" ></script>

    <!-- smooth scroll -->
    <script src="<?=base_url('new_assets/');?>smoothscroll/smooth-scroll.js"></script>


    <!-- scroll top -->
    <script src="js/scrolltotop_arrow_code.js"></script>
    <script src="<?= base_url('assets/');?>js/back-top.js"></script>
    <!-- scroll top -->

    <!-- hover animation card -->
    <script src="<?= base_url('assets/');?>js/jquery.twitch.js"></script>

    <!-- slider carousel apik -->
    <script src="<?=base_url('assets/');?>js/slider.js"></script>

    <!-- image select picker -->
    <script src="<?= base_url('assets/');?>js/image-picker.js"></script>


    <!-- plugin mata uang otomatis -->

    <script src="<?= base_url('assets/'); ?>libs/rupiah.js"></script>



    <!-- live validation -->
    <script>
     (function() {
       'use strict';
       window.addEventListener('load', function() {
         // Fetch all the forms we want to apply custom Bootstrap validation styles to
         var forms = document.getElementsByClassName('needs-validation');
         // Loop over them and prevent submission
         var validation = Array.prototype.filter.call(forms, function(form) {
           form.addEventListener('submit', function(event) {
             if (form.checkValidity() === false) {
               event.preventDefault();
               event.stopPropagation();
             }
             form.classList.add('was-validated');
           }, false);
         });
       }, false);
     })();
   </script>


   <!-- Memberi nama pada form input file -->
     <script>
        $(".custom-file-input").on("change", function() {
           var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
     </script>


     <!-- hover animation card -->
    <script>
      $('.card1').twitch({
      layers: 1,       //Number: the number of layers affected; default is 1
      distance: 1,    //Number: distance in pixels the element will travel; default is 20
      transition: 0.3,
      positionType:'relative'//Number: time in milliseconds for css transitions; default is 0.3
    });
    </script>
    


    <!-- carrousel -->
    <script type="text/javascript">
    $('.carousel').carousel({
    interval: 5000
    })
    </script>
    

    <!-- sorting otomatis -->
    <script>
       $(document).ready(function(){
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
    </script>

   
    <!-- configurasi caption ditengah, carousel bootstrap -->
     <script>
      $(document).ready(function() {
        $('.carousel .carousel-caption').css('zoom', $('.carousel').width()/1250);
      });

      $(window).resize(function() {
        $('.carousel .carousel-caption').css('zoom', $('.carousel').width()/1250);
      });
    </script>


    <!--  tooltip bubble -->
    <script type="text/javascript">
    $('span1').flstBubble({
    theme: { 
      background:'#C0392B',
      color:'#fff' 
    },
    autoscroll:true,
    });
    </script>    


  <!-- pop up gambar apik -->
    <script type="text/javascript">
   $('.galpop-single').galpop();

    </script>

    <!-- slider apik -->
    <script>
        new FgSlider('slider-example', {
        autoplay: true, // autoplay on / off
        effect: 'slide-top', // fade, scale, slide, slide-top
        duration: 5000, // duration till the next slide
        bullets: true, // show / hide bullets
    });
    </script>

   <!-- pop up otomatis / rating --> 
  <script> 
    setTimeout(function() {
        $('#myModal').modal('show');
    }, 10000);
  </script>
  <!-- pop up otomatis tidak bisa di close --> 

  <script> 
    setTimeout(function() {    
      $('#mySquadModal').modal({ 
        backdrop: 'static',
        keyboard: false
      });
    }, 1000);
  </script>

  <script> 
    $(".image-picker").imagepicker({

    // show/hide the regular select box
    hide_select: true,

    // show/hide image labels
    show_label: true,

    // callback functions
    initialized: undefined,
    changed: undefined,
    clicked: undefined,
    selected: undefined,

    // set the max number of selectable options 
    limit: 1,

    // called when the limit cap has been reached
    limit_reached: 1,

    // using Font Awesome icons instead
    font_awesome: true
    
  })
  </script>  

  <!-- script mata uang -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('#angka1').maskMoney();
      $('#angka2').maskMoney({
        prefix: 'US$'
      });
      $('#angka3').maskMoney({
        prefix: 'Rp. ',
        thousands: '.',
        decimal: ',',
        precision: 0
      });
      $('#angka4').maskMoney();
    });
  </script>


</body>

</html>