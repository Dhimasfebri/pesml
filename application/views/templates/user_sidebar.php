<!-- ==============================================================  -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- User Profile-->
                        <li>
                            <!-- User Profile-->
                            <div class="user-profile d-flex no-block dropdown m-t-20">
                                <div class="user-pic"><img src="<?=base_url('assets/images/users/'.$user['image']);?>" alt="users" class="rounded-circle" width="40" /></div>
                                <div class="user-content hide-menu m-l-10">
                                    <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <h5 class="m-b-0 user-name font-medium"><?= $user['name']; ?> <i class="fa fa-angle-down"></i></h5>
                                        <span class="op-5 user-email"><?= $user['email'];?> </span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Userdd">
                                        <?php if($this->session->userdata('role_id') == '2') : ?>
                                        <a class="dropdown-item" href="<?=base_url('user/editprofile');?>"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                        <a class="dropdown-item" href="<?= base_url('user/mySquad');?>"><i class="fas fa-users m-r-5 m-l-5"></i> My Squad </a>
                                        <?php else : ?>
                                         <a class="dropdown-item" href="<?=base_url('admin/editprofile');?>"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                        <?php endif ?>    
                                       <!--  <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                                        <div class="dropdown-divider"></div> -->
                                        
                                        <a class="dropdown-item" href="<?= base_url('auth/logout');?>"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End User Profile-->
                        </li>
                        
                    <?php if($this->session->userdata('role_id') == '2') : ?>
                        <?php if ($this->uri->segment(1) == 'user' && $this->uri->segment(2) != 'recomendation') : ?>
                        <li class="p-15 m-t-10"><a href="<?=base_url('user');?>" class="btn btn-block btn-primary text-white no-block d-flex align-items-center"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu m-l-5">Dashboard</span> </a></li>

                        <?php else : ?>
                        <li class="sidebar-item m-t-10"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?=base_url('user');?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        <?php endif;?>
                    <?php endif; ?>
                     <?php if($this->session->userdata('role_id') == '1') : ?>
                        <?php if ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == '') : ?>
                        <li class="p-15 m-t-10"><a href="<?=base_url('admin');?>" class="btn btn-block btn-primary text-white no-block d-flex align-items-center"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu m-l-5">Dashboard</span> </a></li>

                        <?php else : ?>
                        <li class="sidebar-item m-t-10"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?=base_url('admin');?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        <?php endif;?>
                    <?php endif; ?>
                    <?php if($this->session->userdata('role_id') == '1') : ?>
                       <!--  Management League -->
                        <?php if ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'leaguedata') : ?>
                        <li class="p-15"><a href="<?=base_url('admin/leaguedata');?>" class="btn btn-block btn-primary text-white no-block d-flex align-items-center"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu m-l-5">Management League</span> </a></li>

                        <?php else : ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?=base_url('admin/leaguedata');?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Management League</span></a></li>
                        <?php endif;?>


                         <!--  Management Team -->
                        <?php if ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'league') : ?>
                        <li class="p-15"><a href="<?=base_url('admin/league');?>" class="btn btn-block btn-primary text-white no-block d-flex align-items-center"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu m-l-5">Management Team</span> </a></li>

                        <?php else : ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?=base_url('admin/league');?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Management Team</span></a></li>
                        <?php endif;?>

                        <?php if ($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'player') : ?>
                        <li class="p-15"><a href="<?=base_url('admin/player');?>" class="btn btn-block btn-primary text-white no-block d-flex align-items-center"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu m-l-5">Management Player</span> </a></li>

                        <?php else : ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?=base_url('admin/player');?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Management Player</span></a></li>
                        <?php endif;?>

                    <?php endif; ?>
                    <?php if($this->session->userdata('role_id') == '2') : ?>
                        <?php if ($this->uri->segment(1) == 'user' && $this->uri->segment(2) == 'recomendation') : ?>
                        <li class="p-15 m-t-2"><a href="<?=base_url('user');?>" class="btn btn-block btn-primary text-white no-block d-flex align-items-center"><i class="mdi mdi-magnify-plus"></i><span class="hide-menu m-l-5">Recomendation Player</span> </a></li>

                        <?php else : ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?=base_url('user/recomendation');?>" aria-expanded="false"><i class="mdi mdi-magnify-plus"></i><span class="hide-menu">Recomendation Player</span></a></li>
                        <?php endif;?>

                        <!-- User Profile-->
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false"><i class="mdi mdi-magnify-plus"></i><span class="hide-menu">Advanced Search</span></a></li>
                    <?php endif; ?>
                    
                        <?php if ($this->uri->segment(1) == 'league' ) : ?>
                        <li class="p-15 m-t-2"><a href="<?=base_url('league');?>" class="btn btn-block btn-primary text-white no-block d-flex align-items-center"><i class="mdi mdi-soccer"></i><span class="hide-menu m-l-5">League</span> </a></li>

                        <?php else : ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?=base_url('league');?>" aria-expanded="false"><i class="mdi mdi-soccer"></i><span class="hide-menu">League</span></a></li>
                        <?php endif;?>
                       
                        <!-- <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="icon-material.html" aria-expanded="false"><i class="mdi mdi-face"></i><span class="hide-menu">Icon</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="starter-kit.html" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Blank</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="error-404.html" aria-expanded="false"><i class="mdi mdi-alert-outline"></i><span class="hide-menu">404</span></a></li> -->
                        <!-- <li class="text-center p-40 upgrade-btn">
                            <a href="https://wrappixel.com/templates/xtremeadmin/" class="btn btn-block btn-danger text-white" target="_blank">Upgrade to Pro</a>
                        </li> -->
                    </ul>
                    
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        