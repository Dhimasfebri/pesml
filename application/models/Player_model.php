<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Player_model extends CI_Model {

	private $_table ="player";
	public $id_player;
	public $id_position;
	public $name;
	public $id_team;
	public $height;
	public $age;
	public $overall;
	public $market_value;
	public $salaries;
	public $id_player_role;
	
	public function getTotal()
	{   
		$query = $this->db->get('player');
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}

	public function getAll() //sementara seri-A
	{   
		return $this->db->get($this->_table)->result();
	}





	public function searchplayer(){ //Search player
		
		$this->db->select('name,market_value,overall,team_name,league_name');
		$this->db->from('player');
		$this->db->join('team', 'team.id_team = player.id_team');
		$this->db->join('league', 'team.id_league = league.id_league');
		$this->db->like('name', $this->input->post('player'));
		$query  = $this->db->get();
		return $query->result();
	}
	
	public function getAllteam($id_team){ //team controler

		
		return $this->db->query("SELECT * FROM `player` 
			JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
			JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
			JOIN `league` ON `team`.`id_league` = `league`.`id_league`
			JOIN `recomendation` ON `recomendation`.`id_player` = `player`.`id_player`  
			WHERE team.team_name = '$id_team'  
			ORDER BY `player`.`id_position` ASC")->result();
	}

	public function getAllteamrow($id_team){ //team controler menampilkan semua pemain dari tim yg dipilih

		
		return $this->db->query("SELECT * FROM `player` 
			JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
			JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
			JOIN `league` ON `team`.`id_league` = `league`.`id_league` 
			JOIN `recomendation` ON `recomendation`.`id_player` = `player`.`id_player` 
			WHERE team.team_name = '$id_team'")->row_array();
	}

	// pake nama
	// public function getByIdPlayer($id_player){
	// 	$name = urldecode($id_player);
	// 	return $this->db->query("SELECT * FROM `player` 
	// 		JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
	// 		JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
	// 		JOIN `league` ON `team`.`id_league` = `league`.`id_league` 
	// 		JOIN `player_role` ON `player`.`id_player_role` = `player_role`.`id_player_role` 
	// 		JOIN `recomendation` ON `recomendation`.`id_player` = `player`.`id_player` 
	// 		WHERE player.name = '$name'")->row_array();
	// }

	// pake id
	public function getByIdPlayer($id_player){
		
		return $this->db->query("SELECT * FROM `player` 
			JOIN `team` ON `player`.`id_team` = `team`.`id_team` 
			JOIN `position` ON `player`.`id_position` = `position`.`id_position` 
			JOIN `league` ON `team`.`id_league` = `league`.`id_league` 
			JOIN `player_role` ON `player`.`id_player_role` = `player_role`.`id_player_role` 
			JOIN `recomendation` ON `recomendation`.`id_player` = `player`.`id_player` 
			WHERE player.id_player = '$id_player'")->row_array();
	}



	public function getBestPlayer(){
		
		return $this->db->query("SELECT * FROM `player` JOIN `team` ON `player`.`id_team` = `team`.`id_team` JOIN `position` ON `player`.`id_position` = `position`.`id_position` JOIN `league` ON `team`.`id_league` = `league`.`id_league` where overall between '85' and '100' ORDER BY `player`.`overall` DESC limit 12 ")->result();
	}

	public function getValuablePlayer($id_league){
		
		return $this->db->query("SELECT * FROM `player`JOIN `team` ON `player`.`id_team` = `team`.`id_team` WHERE team_name = '$id_league' ORDER BY `player`.`market_value` DESC limit 1 ")->row_array();
	}

	public function getTotalPlayerFromTeam($id_league){
		
		return $this->db->query("SELECT count(name) AS total FROM `player` JOIN `team` ON `player`.`id_team` = `team`.`id_team` WHERE team_name = '$id_league'")->row_array();
	}
	


	public function addplayer(){
		// $this->db->trans_begin();
		// $password = htmlspecialchars($this->input->post('password'));
		// $password2 = hash('sha256', $password);//enkripsi sha-256

		$data = [
			
			"id_position" 		=> htmlspecialchars($this->input->post('id_position')),
			"name" 	=> htmlspecialchars($this->input->post('name', true)),
			"id_team" 	=> htmlspecialchars($this->input->post('id_team', true)),
			"height" 	=> htmlspecialchars($this->input->post('height', true)),
			"age" 	=> htmlspecialchars($this->input->post('age', true)),
			"name" 	=> htmlspecialchars($this->input->post('name', true)),
			"overall" 	=> htmlspecialchars($this->input->post('overall', true)),
			"market_value" 	=> htmlspecialchars($this->input->post('market_value', true)),
			"salaries" 	=> htmlspecialchars($this->input->post('salaries', true)),
			"id_player_role" 	=> htmlspecialchars($this->input->post('id_player_role', true)),
			
			// "password" 	=> $password2,
			
			];

			$this->db-> insert('player', $data);

			// 
	} 

	public function editplayer(){
		// $this->db->trans_begin();
		// $password = htmlspecialchars($this->input->post('password'));
		// $password2 = hash('sha256', $password);//enkripsi sha-256

		$data = [
			
			"id_position" 		=> htmlspecialchars($this->input->post('id_position')),
			"name" 	=> htmlspecialchars($this->input->post('name', true)),
			"id_team" 	=> htmlspecialchars($this->input->post('id_team', true)),
			"height" 	=> htmlspecialchars($this->input->post('height', true)),
			"age" 	=> htmlspecialchars($this->input->post('age', true)),
			"name" 	=> htmlspecialchars($this->input->post('name', true)),
			"overall" 	=> htmlspecialchars($this->input->post('overall', true)),
			"market_value" 	=> htmlspecialchars($this->input->post('market_value', true)),
			"salaries" 	=> htmlspecialchars($this->input->post('salaries', true)),
			"id_player_role" 	=> htmlspecialchars($this->input->post('id_player_role', true)),
			
			// "password" 	=> $password2,
			
			];

			$this->db-> where('id_player', $this->input->post('id'));
			$this->db-> update('player', $data);

			// 
	} 
	public function deleteplayer($id){
	}


}
