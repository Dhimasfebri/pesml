<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	private $_table ="user";
	public $id_user;
	public $name;
	public $password;
	public $image;
	public $date_created;
	
	public function getTotal()
	{   
		
		$query = $this->db->get_where('user', ['role_id' => 2]);
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}

	public function getAll()
	{
		return $this->db->get_where($this->_table, ['role_id'=> 2])->result();
	}
	
	public function getByIdUser($id){
		
		return $this->db->get_where($this->_table, ["id_user"=>$id])->row_array();
	}
	public function editprofile(){
		// $this->db->trans_begin();
		// $password = htmlspecialchars($this->input->post('password'));
		// $password2 = hash('sha256', $password);//enkripsi sha-256

		$data = [
			
			"name" 		=> htmlspecialchars($this->input->post('name')),
			"email" 	=> htmlspecialchars($this->input->post('email', true)),
			
			// "password" 	=> $password2,
			
			];

			$this->db-> where('id_user', $this->input->post('id'));
			$this->db-> update('user', $data);

			// 

	} 



}
