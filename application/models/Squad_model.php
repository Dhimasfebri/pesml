<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Squad_model extends CI_Model {

	private $_table ="squad";
	public $id_user;
	public $id_team;
	public $transfer_budget;
	public $salary_budget;
	public $total_player_u21;
	public $total_player_over_u29;
	public $total_player_u21_u29;
	
	
	public function getAllSquad()
	{   
		$query = $this->db->get('squad');
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
	
	
	public function getMySquad(){ //team controler

		$email = $this->session->userdata('email');
		return $this->db->query("SELECT * FROM `squad` 
			JOIN `team` ON `squad`.`id_team` = `team`.`id_team` 
			JOIN `user` ON `squad`.`email` = `user`.`email` 
			WHERE user.email = '$email'  
			")->row_array();
	}

	
	public function addSquad(){
		// $this->db->trans_begin();
		// $password = htmlspecialchars($this->input->post('password'));
		// $password2 = hash('sha256', $password);//enkripsi sha-256

		

		$data = [
			
			
			"email" 	=> htmlspecialchars($this->session->userdata('email')),
			"id_team" 	=> htmlspecialchars($this->input->post('id_team', true)),
			"transfer_budget" 	=> 0,
			"salary_budget" 	=> 0,
			"total_age_less_21" 	=> 0,
			"total_age_21to29" 	=> 0,
			"total_age_over_29" 	=> 0,
			
			];

			$this->db-> insert('squad', $data);

			// 
	} 

	public function editSquad(){
		// $this->db->trans_begin();
		// $password = htmlspecialchars($this->input->post('password'));
		// $password2 = hash('sha256', $password);//enkripsi sha-256

		$data = [
			
			"transfer_budget" 	=> htmlspecialchars($this->input->post('transfer_budget', true)),
			"salary_budget" 	=> htmlspecialchars($this->input->post('salary_budget', true)),
			"total_age_less_21" 	=> htmlspecialchars($this->input->post('total_age_less_21', true)),
			"total_age_21to29" 	=> htmlspecialchars($this->input->post('total_age_21to29', true)),
			"total_age_over_29" 	=> htmlspecialchars($this->input->post('total_age_over_29', true)),
	
			];

			$this->db->where('email', $this->session->userdata('email'));
			$this->db->update('squad', $data);

			// 
	}

	public function deleteMySquad(){
		// $this->db->trans_begin();
		// $password = htmlspecialchars($this->input->post('password'));
		// $password2 = hash('sha256', $password);//enkripsi sha-256

		

		$email = $this->session->userdata('email');
		$this->db->delete('squad', ['email' => $email]);

			// 
	} 


}
