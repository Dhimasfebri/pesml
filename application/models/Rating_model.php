<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rating_model extends CI_Model {

	public function rating_user(){ 
		$email = $this->session->userdata('email');

		$this->db->select('*');
		$this->db->from('rating');
		$this->db->join('player', 'rating.id_player = player.id_player');
		$this->db->join('team', 'team.id_team = player.id_team');
		$this->db->join('league', 'team.id_league = league.id_league');		
		$this->db->where('email',$email);
		$this->db->order_by('date_created', 'DESC');
		$query  = $this->db->get();
		return $query->result();		
	}
	
}


		