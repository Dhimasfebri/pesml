<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team_model extends CI_Model {

	private $_table ="team";
	public $id_team;
	public $id_league;
	public $team_name;
	public $ket;
	public $image;
	

	public function getAll(){

		return $this->db->get($this->_table)->result();
	}
	public function getAllSeria(){

		return $this->db->get_where($this->_table,['id_league' => '2'])->result();
		
	}

	public function getTotal()
	{   
		$query = $this->db->get('team');
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
	

	public function searchteam(){

		$this->db->select('team_name,id_team,team.id_league,img_team,league_name');
		$this->db->from('team');
		$this->db->join('league', 'team.id_league = league.id_league');
		$this->db->like('team_name', $this->input->post('player'));
		$this->db->limit(12);
		$query = $this->db->get();
		return $query->result();
	}

	public function getByIdteam($id_league){
		
		$this->db->select('*');
		$this->db->from('league');
		$this->db->join('team', 'team.id_league = league.id_league');
		$this->db->where('league.league_name', $id_league);
		$query = $this->db->get();

		return $query->result();
	}
	public function getByIdteam1($id_team){
		
		$this->db->select('*');
		$this->db->from('league');
		$this->db->join('team', 'team.id_league = league.id_league');
		$this->db->where('league.league_name', $id_team);
		$query = $this->db->get();

		return $query->result();
	}

	
}