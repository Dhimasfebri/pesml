<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class League_model extends CI_Model {

	private $_table ="league";
	public $id_league;
	public $league_name;
	public $image;
	
	public function getAll(){

		return $this->db->get($this->_table)->result();
	}

	public function getTotal()
	{   
		$query = $this->db->get('league');
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
	
	public function getByIdleague($id_league){
		
		return $this->db->query("SELECT * FROM `league` JOIN `team` ON `league`.`id_league` = `team`.`id_league`WHERE `league`.`league_name` = '$id_league' ")->row_array();
	}


	public function getByIdleagueresult($id_league){
		
		return $this->db->query("SELECT * FROM `league` JOIN `team` ON `league`.`id_league` = `team`.`id_league`WHERE `league`.`league_name` = '$id_league' ")->result();
	}

	public function getByIdleague1($id_team){
		
		return $this->db->query("SELECT * FROM `league` JOIN `team` ON `league`.`id_league` = `team`.`id_league`WHERE `league`.`league_name` = '$id_team' ")->row_array();
	}
	public function getByIdleagueresult1($id_team){
		
		return $this->db->query("SELECT * FROM `league` JOIN `team` ON `league`.`id_league` = `team`.`id_league`WHERE `league`.`league_name` = '$id_team' ")->result();
	}


}