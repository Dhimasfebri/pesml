<?php
class Admin extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role_id') != 1) {
			redirect('admin');
		}

		is_logged_in();
		$this->load->library('form_validation');
		$this->load->model("User_model");
		$this->load->model("Player_model");
		$this->load->model("League_model");
		$this->load->model("Team_model");
		$this->load->model("Rating_model");
	}

		

	public function index()
	{
		$data['title'] = 'Dashboard';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['users'] = $this->User_model->getAll();
		$data['total'] = $this->User_model->getTotal();
		$data['total2'] = $this->Team_model->getTotal();
		$data['total3'] = $this->League_model->getTotal();
		$data['total4'] = $this->Player_model->getTotal();

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('admin/index',$data);
		$this->load->view('templates/user_footer',$data);
	}

	
	public function editprofile()
	{
		$data['title'] = 'Edit Profile';
		$data["user"] = $this->db->get_where('user',['email' => $this->session->userdata('email')])->row_array();
		$data['rating'] = $this->Rating_model->rating_user();
		$this->form_validation->set_rules('name','Name', 'required|trim');
		// $this->form_validation->set_rules('email','Email', 'required|trim|valid_email');
		// $this->form_validation->set_rules('pass1','Last Password', 'required|trim|min_length[3]|matches[pass0]',[
		// 	'matches' => 'Password dont match!',
		// 	'min_length' => 'Password too short!'
		// 	]);

		// $this->form_validation->set_rules('pass0','Last Password', 'trim|matches[pass1]');
		// $this->form_validation->set_rules('pass1','Password', 'required|trim|min_length[3]',[
		// 	'min_length' => 'Password too short!'
		// 	]);

		if($this->form_validation->run() == false) {
			$this->load->view('templates/user_header',$data);
			$this->load->view('templates/user_sidebar',$data);
			$this->load->view('admin/editprofile',$data);
			$this->load->view('templates/user_footer',$data);
		}else{
			
			$data['user']=$this->User_model->editprofile();
			$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your account profile has been change.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('admin/editprofile');
		}
		// echo 'welcome '. $data['user']['name'];	
	}

	public function editpass()
	{
		$data['title'] = 'Edit Profile';
		$data["user"] = $this->db->get_where('user',['email' => $this->session->userdata('email')])->row_array();
		$data['rating'] = $this->Rating_model->rating_user();

		$this->form_validation->set_rules('current_password','Current Password','required|trim');
		$this->form_validation->set_rules('new_password1','Password', 'required|trim|min_length[3]|matches[new_password2]',[
			'min_length' => 'Password too short!'
			]);
		$this->form_validation->set_rules('new_password2','Confirm Password', 'required|trim|min_length[3]|matches[new_password1]',[
			'min_length' => 'Password too short!'
			]);

		if($this->form_validation->run() == false) {
			$this->load->view('templates/user_header',$data);
			$this->load->view('templates/user_sidebar',$data);
			$this->load->view('admin/editprofile',$data);
			$this->load->view('templates/user_footer',$data);
		}else{
			$current_password = $this->input->post('current_password');
			$new_password = $this->input->post('new_password1');
			if(!password_verify($current_password, $data['user']['password'])){
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Wrong current password!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
				redirect('admin/editprofile');

			}
			else {
				if($current_password == $new_password){
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>New password cannot be the same as current !</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
				redirect('admin/editprofile');

				}
				else{
					$password_hash = password_hash($new_password, PASSWORD_DEFAULT);
					$this->db-> set('password', $password_hash);
					$this->db-> where('email', $this->session->userdata('email'));
					$this->db-> update('user');
					$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your password account has been change.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
					</button>
						</div>');
					redirect('admin/editprofile');
				}


			}
		}
		// echo 'welcome '. $data['user']['name'];	
	}


	public function editpp(){

	$data["user"] = $this->db->get_where('user',['email' => $this->session->userdata('email')])->row_array();
	// if($this->form_validation->run() == false) {
	// 		$this->load->view('templates/user_header',$data);
	// 		$this->load->view('templates/user_sidebar',$data);
	// 		$this->load->view('user/editprofile',$data);
	// 		$this->load->view('templates/user_footer',$data);
	// 	}else{
	$email = $this->session->userdata('email');
			// cek jika ada gambar
			$upload_image =$_FILES['image']['name'];
			// var_dump($upload_image);
			// die;
			 if ($upload_image){
			 	$config['allowed_types'] = 'gif|jpg|png';
			 	$config['max_size'] = '2048';
			 	$config['upload_path'] = './assets/images/users/';

			 	$this->load->library('upload',$config);
			 	if ($this->upload->do_upload('image')){
			 		$old_image = $data['user']['image'];
			 		if($old_image != 'default.jpg'){
			 			unlink(FCPATH . 'assets/images/users/'. $old_image);
			 		}

			 		$new_image = $this->upload->data('file_name');


			 		$this->db->set('image', $new_image);
			 		
					
		 	} else{
			 	$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>'.$this->upload->display_errors().'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('admin/editprofile');
			 // 		$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' .$this->upload->display_errors() .'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				// </button></div>');
				// 	redirect('user/editprofile');
			 	}


			 }
			// $this->db->set('image', $upload_image);
			$this->db->where('email', $email);
			$this->db->update('user');
			// $data['user']=$this->User_model->editpp();
			$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your account profile has been change.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('admin/editprofile');
		// echo 'welcome '. $data['user']['name'];	
		
	}

	public function edituser()
	{

	$data['title'] = 'Edit User';
	$this->load->view('admin/edituser',$data);
	}

	public function leaguedata()// daftar management liga
	{
		$data['title'] = 'League data';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['league'] = $this->League_model->getAll();

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('admin/league_data',$data);
		$this->load->view('templates/user_footer',$data);
	}

	public function league()// menampilkan daftar liga
	{
		$data['title'] = 'League';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['league'] = $this->League_model->getAll();
		
		
		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('admin/league',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}

	public function getTeambyLeagueid($id_league)
	{
		$data['title'] = 'League Page';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data["league"] = $this->League_model->getByIdleague($id_league);
		$data["league2"] = $this->League_model->getByIdleagueresult($id_league);
		
		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('admin/team_data',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}
	public function player()
	{
		
	}
	public function editLeague()
	{

	$data['title'] = 'Edit Team';
	$this->load->view('admin/editplayer',$data);
	}

	public function editTeam()
	{

	$data['title'] = 'Edit Team';
	$this->load->view('admin/editplayer',$data);
	}

	public function editPlayer()
	{

	$data['title'] = 'Edit Player';
	$this->load->view('admin/editplayer',$data);
	}


	public function about()
	{

	$data['title'] = 'About';
	$this->load->view('admin/about',$data);
	
	
	}
}
?>