<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{
	public function __construct()
	{

		parent::__construct();

		if ($this->session->userdata('role_id') != 2) {
			redirect('admin');
		}

		is_logged_in();

		//menghilangkan resubmission//
		header('Last-Modifed:'. gmdate('D, d M Y H:i:s'). 'GMT');
		header('Cache-Control: no-cache, must-revalidate, max-age=0');
		header('Cache-Control: post-check=0, pre-check=0', false);
		header('Pragma: no-cache');
		
		$this->load->library('form_validation');
		$this->load->model("User_model");
		$this->load->model("Player_model");
		$this->load->model("League_model");
		$this->load->model("Team_model");
		$this->load->model("Rating_model");
		$this->load->model("Squad_model");

		$data['squad'] = $this->db->get_where('squad', ['email' => $this->session->userdata('email')])->row_array();
		$data["team"] = $this->Team_model->getAllSeria();
		if ($this->uri->segment(2) != 'addSquad'  && !$data['squad'] && $this->uri->segment(2) != '' ) {
			redirect('user');
		}


	}

	public function index()
	{
		$data['title'] = 'Dashboard';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['player'] = $this->Player_model->getBestPlayer();
		$data['squad'] = $this->db->get_where('squad', ['email' => $this->session->userdata('email')])->row_array();
		$data["team"] = $this->Team_model->getAllSeria();

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('user/index',$data);
		$this->load->view('templates/user_footer',$data);
		
	}

	public function mySquad()
	{
		$data['title'] = 'My Squad';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['myteam'] = $this->Squad_model->getMySquad();
		
		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('user/squad',$data);
		$this->load->view('templates/user_footer',$data);
		
		
	}

	public function addSquad()
	{
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		if(!$_POST){
			redirect('user');
		}else{
			$data['user']=$this->Squad_model->addSquad();		
			$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your Squad Team has been added, to check your squad ,you can click your photo profile and then click squad.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('user');
		}
			
	}

	public function editMySquad()
	{
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		if(!$_POST){
			redirect('user/mySquad');
		}else{
			// var_dump($_POST);
			// die;
			$data['user']=$this->Squad_model->editSquad();		
			$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your Squad Team has been edited<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('user/mySquad');
		}
			
	}

	public function deleteMySquad()
	{
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		
			$data['user']=$this->Squad_model->deleteMySquad();		
			$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your Squad Team data has been delete<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('user');
		
			
	}

	

	public function getplayer()
	{
		$data['title'] = 'Dashboard';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['player'] = $this->Player_model->searchplayer();
		$data['team'] = $this->Team_model->searchteam();

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('user/search',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}
	public function recomendation()
	{
		$data['title'] = 'Recomendation';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['player'] = $this->Player_model->getBestPlayer();
		$data['myteam'] = $this->Squad_model->getMySquad();

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('user/recomendation',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}

	public function rating()
	{
		$data['title'] = 'Recomendation';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['player'] = $this->Player_model->getBestPlayer();

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('user/rating',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}


	public function result()
	{
		$data['player_roles'] = $this->db->query('Select * from player_role')->result();
		
		if($_POST['transfer_budget'] == "0" && $_POST['salary_budget'] == "0"){
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Please edit your transfer budget and salary_budget first! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('user/recomendation');
		}

		$this->load->model("Recomendation_model");
		$data['title'] = 'Recomendation';
		$data['myteam'] = $this->Squad_model->getMySquad();
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['rating'] = $this->db->get_where('rating', ['email' => $this->session->userdata('email')])->row_array();
		$data['myposition'] = $this->db->get('position')->result();
		// $data['recomendation'] = $this->Recomendation_model->getRecomendation();

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('user/result',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}
	// public function row($key)
	// {
	// 	$this->load->model("Recomendation_model");
	// 	$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
	// 	$data['row'] = $this->Recomendation_model->get_recomend_by_id($key);

	// 	$this->load->view('templates/user_header',$data);
	// 	$this->load->view('templates/user_sidebar',$data);
	// 	$this->load->view('user/result',$data);
	// 	$this->load->view('templates/user_footer',$data);
		
	// 	// echo 'welcome '. $data['user']['name'];
	// }


	public function editprofile()
	{
		$data['title'] = 'Edit Profile';
		$data["user"] = $this->db->get_where('user',['email' => $this->session->userdata('email')])->row_array();
		$data['rating'] = $this->Rating_model->rating_user();
		$data['myteam'] = $this->Squad_model->getMySquad();
		$this->form_validation->set_rules('name','Name', 'required|trim');
		
		if($this->form_validation->run() == false) {
			$this->load->view('templates/user_header',$data);
			$this->load->view('templates/user_sidebar',$data);
			$this->load->view('user/editprofile',$data);
			$this->load->view('templates/user_footer',$data);
		}else{
			
			$data['user']=$this->User_model->editprofile();
			$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your account profile has been change.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('user/editprofile');
		}
		// echo 'welcome '. $data['user']['name'];	
	}

	public function editpass()
	{
		$data['title'] = 'Edit Profile';
		$data["user"] = $this->db->get_where('user',['email' => $this->session->userdata('email')])->row_array();
		$data['rating'] = $this->Rating_model->rating_user();

		$this->form_validation->set_rules('current_password','Current Password','required|trim');
		$this->form_validation->set_rules('new_password1','Password', 'required|trim|min_length[3]|matches[new_password2]',[
			'min_length' => 'Password too short!'
			]);
		$this->form_validation->set_rules('new_password2','Confirm Password', 'required|trim|min_length[3]|matches[new_password1]',[
			'min_length' => 'Password too short!'
			]);

		if($this->form_validation->run() == false) {
			$this->load->view('templates/user_header',$data);
			$this->load->view('templates/user_sidebar',$data);
			$this->load->view('user/editprofile',$data);
			$this->load->view('templates/user_footer',$data);
		}else{
			$current_password = $this->input->post('current_password');
			$new_password = $this->input->post('new_password1');
			if(!password_verify($current_password, $data['user']['password'])){
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Wrong current password!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
				redirect('user/editprofile');

			}
			else {
				if($current_password == $new_password){
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>New password cannot be the same as current !</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
				redirect('user/editprofile');

				}
				else{
					$password_hash = password_hash($new_password, PASSWORD_DEFAULT);
					$this->db-> set('password', $password_hash);
					$this->db-> where('email', $this->session->userdata('email'));
					$this->db-> update('user');
					$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your password account has been change.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
					</button>
						</div>');
					redirect('user/editprofile');
				}


			}
		}
		// echo 'welcome '. $data['user']['name'];	
	}


	public function editpp(){

	$data["user"] = $this->db->get_where('user',['email' => $this->session->userdata('email')])->row_array();
	// if($this->form_validation->run() == false) {
	// 		$this->load->view('templates/user_header',$data);
	// 		$this->load->view('templates/user_sidebar',$data);
	// 		$this->load->view('user/editprofile',$data);
	// 		$this->load->view('templates/user_footer',$data);
	// 	}else{
	$email = $this->session->userdata('email');
			// cek jika ada gambar
			$upload_image =$_FILES['image']['name'];
			// var_dump($upload_image);
			// die;
			 if ($upload_image){
			 	$config['allowed_types'] = 'gif|jpg|png';
			 	$config['max_size'] = '2048';
			 	$config['upload_path'] = './assets/images/users/';

			 	$this->load->library('upload',$config);
			 	if ($this->upload->do_upload('image')){
			 		$old_image = $data['user']['image'];
			 		if($old_image != 'default.jpg'){
			 			unlink(FCPATH . 'assets/images/users/'. $old_image);
			 		}

			 		$new_image = $this->upload->data('file_name');


			 		$this->db->set('image', $new_image);
			 		
					
			 		
			 		

			 	} else{
				 	$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>'.$this->upload->display_errors().'</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
					</button>
					</div>');
				redirect('user/editprofile');
			 // 		$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' .$this->upload->display_errors() .'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				// </button></div>');
				// 	redirect('user/editprofile');
			 	}


			 }
			// $this->db->set('image', $upload_image);
			$this->db->where('email', $email);
			$this->db->update('user');
			// $data['user']=$this->User_model->editpp();
			$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert"><strong>Congratulation!</strong> Your account profile has been change.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				</div>');
			redirect('user/editprofile');
		// echo 'welcome '. $data['user']['name'];	
		
	}

	public function about(){

	$data['title'] = 'About';
	$this->load->view('user/about',$data);
	
	
	}
}


