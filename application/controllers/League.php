<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class League extends CI_Controller 
{
	public function __construct(){


		parent::__construct();

		is_logged_in();

		$this->load->model("League_model");
		$this->load->model("User_model");
		$this->load->model("Team_model");
		$this->load->model("Player_model");

		$data['squad'] = $this->db->get_where('squad', ['email' => $this->session->userdata('email')])->row_array();
		$data["team"] = $this->Team_model->getAllSeria();
		if ($this->uri->segment(1) != 'user' && !$data['squad']) {
			redirect('user');
		}
	
		
	}


	public function index()// menampilkan daftar liga
	{
		$data['title'] = 'League';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['league'] = $this->League_model->getAll();
		
		
		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('league/index',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}
	public function getTeambyLeagueid($id_league) //menampilkan daftar team
	{
		$data['title'] = 'League Page';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data["league"] = $this->League_model->getByIdleague($id_league);
		$data["league2"] = $this->League_model->getByIdleagueresult($id_league);
		
		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('league/league',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}

	public function getPlayersbyTeam($id_league)//menampilkan pemain dari tim yand dipilih
	{
		$data['title'] = 'League Page';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data["player"] = $this->Player_model->getAllteam($id_league);
		$data["players"] = $this->Player_model->getAllteamrow($id_league);
		$data["total"] = $this->Player_model->getTotalPlayerFromTeam($id_league);
		$data["value"] = $this->Player_model->getValuablePlayer($id_league);
		// 

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('league/team',$data);
		$this->load->view('templates/user_footer',$data);
		// $this->load->view('league/sort',$data);
		
		
		// echo 'welcome '. $data['user']['name'];
	}
	public function sort($id_team)//menampilkan pemain dari tim yand dipilih
	{
		$data['title'] = 'League Page';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data["player"] = $this->Player_model->getAllteamrow($id_team);

		// 

		
		$this->load->view('league/sort2',$data);
		
		// $this->load->view('league/sort',$data);
		
		
		// echo 'welcome '. $data['user']['name'];
	}

	public function getplayersDetails($id_player)//meampilkan detail pemain
	{
		$data['title'] = 'League Page';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data["player"] = $this->Player_model->getByIdPlayer($id_player);

		$this->load->view('templates/user_header',$data);
		$this->load->view('templates/user_sidebar',$data);
		$this->load->view('league/player',$data);
		$this->load->view('templates/user_footer',$data);
		
		// echo 'welcome '. $data['user']['name'];
	}


}